<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
		<title>Healthy Food </title>
		<meta name="description" content="">
		<meta name="author" content="">
                <link rel="shortcut icon" href="images/fevicon.png">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                 <?php include './includes/cssfiles.php';?> 
                <style>
                    .btn-default-transparent:active,.btn-default-transparent:focus{
                        background:none;
                            color: #777;
                    }
                    .tips {
                    display: inline-block;
                    position: relative;
                    width: auto;
                    min-width: 180px;
                    height: auto;
                    vertical-align: top;
                    margin-left: 35px;
                    margin-right: 35px;
                    margin-bottom: 50px;
                    }
                    .innerTip {
                    display: inline-block;
                    vertical-align: middle;
                    width: 100%;
                    margin-left:auto;
                    margin-right:auto;
                    text-align:center;
                    }
                    .innerTipTitle {
                    font-family:century gothic;
                    font-weight:bold;
                    
                    color: #007833;
                    font-size: 16px;
                    line-height: 20px;
                    text-align: center;
                    text-decoration: none;
                    vertical-align: top;
                    padding-top: 10px;
                    }
                    #steps li{
                        font-family:century gothic;
                        font-size:24px;
                        font-weight:bold;
                        
                    }
                    #steps li a{
                        font-family:century gothic;
                        font-size:16px;
                        font-weight:bold;
                        background:transparent;
                       
                       
                    }
                    #steps li a:active{
                        background:orangered;
                        color:white;
                         background:transparent;
                    }
                    #stepContent{
                        position:relative;
                        top:30px;
                        background:#e9ebee;
                    }
                    #stepImg{
                        cursor:pointer;
                    }
                    #stepImg_2{
                        cursor:pointer;
                        height: 200px;
                    }
                    #stepContentInner{
                        background:#e9ebee;
                    }
                                    #footer-top {
                                    background: #0d5995;
                                    min-height: 40px;
                                    border-bottom: 1px solid white;
                                    height: auto;
                                    color: white;
                                    margin-top: 1px;
                                    padding-top: 15px;
                                    box-shadow: inset 0 4px 10px rgba(0,0,0, 0.50);
                                    border-bottom: 1px solid white;
                                    }
                                    #menuBar{
                                        position:relative;
                                        float:right;
                                        top:3px;
                                        font-size:16px;
                                        color:black;
                                        cursor:pointer;
                                        
                                    }
                                    #menuBar:hover{
                                        color:#0d5995;
                                        
                                    }
                                    #request{
                                        position:relative;
                                        
                                        margin-right:10px;
                                        margin-left:10px;
                                        font-weight:bold;
                                    }
                                    .scrollToDown{
                                        text-align: center;
                                        color: #fff;
                                        font-size:21px;
                                        font-weight:bold;
                                        position: fixed;
                                        padding-top:10px;
                                        bottom:60px;
                                        right:5px;
                                        width: 50px;
                                        height: 50px;
                                        cursor: pointer;
                                        background-color: rgba(0,0,0,.4);
                                        z-index: 1005;
                                        -webkit-backface-visibility: hidden;
                                        -webkit-transform: translateZ(0);
                                        -webkit-transition: all .2s ease-in-out;
                                        -o-transition: all .2s ease-in-out;
                                        transition: all .2s ease-in-out;
                                    }


                                
                </style>
	</head>

	
	<body class="no-trans  ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
                   
			<!-- Offcanvas side start -->
                       
			<!-- offcanvas side end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">
                            <?php include 'includes/header.php';?>
			<div class="banner dark-translucent-bg" style="background-image:url('images/foodbanner.jpg'); background-position: 50% 32%;">
				<a href="#homeRow1" id="scrollDownButton"><div class="scrollToDown circle"  style="display: block;"><i class="icon-down-open-big"></i></div></a>
				<div class="container">
					<div class="row">
						<div class="col-md-8 text-center col-md-offset-2 pv-20">
							<h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100"><strong>Order delicious B-Nutrified food online</strong></h2>
							<div class="separator object-non-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
							<p class="text-center object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">Order food online from the best Super kitchen near you.</p>
						</div>
					</div>
				</div>
			</div>
			<!-- banner end -->

			<!-- main-container start -->
			<!-- ================ -->
			<section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            

				<div class="container">
                                    <header class="header  clearfix" style="    margin: -15px;">
					
					<div class="container">
						<div class="row">
							
							<div class="col-md-12">
					
								<!-- header-right start -->
								<!-- ================ -->
								<div class="header-right clearfix">
									
								<!-- main-navigation start -->
								<!-- classes: -->
								<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
								<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
								<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
								<!-- ================ -->
								<div class="main-navigation  animated with-dropdown-buttons">

									<!-- navbar start -->
									<!-- ================ -->
									<nav class="navbar navbar-default" role="navigation">
										<div class="container-fluid">
											<div class="navbar-header">
												<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
												
											</div>
											<div class="navbar-collapse collapse" id="navbar-collapse-1" aria-expanded="false" style="height: 1px;">
                                                                                            <ul class="nav navbar-nav" id="food_category_details_header" style="width: 122%;">
													
												</ul>
											</div>
										</div>
									</nav>
								</div>
								</div>
					
							</div>
						</div>
					</div>
					
				</header>
                                    <div class="row" style="margin-top: 50px;" id="food_category_details_body">
                                                <h3 class="title"><a href="portfolio-item.html">Project Title</a></h3><div class="separator-2"></div>
						<div class="main col-md-12">
							<div class="row">

							</div>

						</div>

					</div>
				</div>
			</section>
<?php include 'includes/footer.php'; ?>
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<?php include 'includes/jsfiles.php'; ?>
                <script type="text/javascript" src="ajax/QuickQuery.js"></script>
                <script type="text/javascript" src="ajax/SelFoodDetails.js"></script>
                <?php include 'includes/support.php'; ?>
        <script>
            $(document).ready(function() {
               SelFoodCategoryDetails();
            });
        </script>
            
        
</html>


