<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
		<title>About B-Nutrified</title>
		<meta name="description" content="">
		<meta name="author" content="">
                <link rel="shortcut icon" href="images/fevicon.png">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                 <?php include './includes/cssfiles.php';?> 
                <style>
                    .btn-default-transparent:active,.btn-default-transparent:focus{
                        background:none;
                            color: #777;
                    }
                    .tips {
                    display: inline-block;
                    position: relative;
                    width: auto;
                    min-width: 180px;
                    height: auto;
                    vertical-align: top;
                    margin-left: 35px;
                    margin-right: 35px;
                    margin-bottom: 50px;
                    }
                    .innerTip {
                    display: inline-block;
                    vertical-align: middle;
                    width: 100%;
                    margin-left:auto;
                    margin-right:auto;
                    text-align:center;
                    }
                    .innerTipTitle {
                    font-family:century gothic;
                    font-weight:bold;
                    
                    color: #007833;
                    font-size: 16px;
                    line-height: 20px;
                    text-align: center;
                    text-decoration: none;
                    vertical-align: top;
                    padding-top: 10px;
                    }
                    #steps li{
                        font-family:century gothic;
                        font-size:24px;
                        font-weight:bold;
                        
                    }
                    #steps li a{
                        font-family:century gothic;
                        font-size:16px;
                        font-weight:bold;
                        background:transparent;
                       
                       
                    }
                    #steps li a:active{
                        background:orangered;
                        color:white;
                         background:transparent;
                    }
                    #stepContent{
                        position:relative;
                        top:30px;
                        background:#e9ebee;
                    }
                    #stepImg{
                        cursor:pointer;
                    }
                    #stepImg_2{
                        cursor:pointer;
                        height: 200px;
                    }
                    #stepContentInner{
                        background:#e9ebee;
                    }
                                    #footer-top {
                                    background: #0d5995;
                                    min-height: 40px;
                                    border-bottom: 1px solid white;
                                    height: auto;
                                    color: white;
                                    margin-top: 1px;
                                    padding-top: 15px;
                                    box-shadow: inset 0 4px 10px rgba(0,0,0, 0.50);
                                    border-bottom: 1px solid white;
                                    }
                                    #menuBar{
                                        position:relative;
                                        float:right;
                                        top:3px;
                                        font-size:16px;
                                        color:black;
                                        cursor:pointer;
                                        
                                    }
                                    #menuBar:hover{
                                        color:#0d5995;
                                        
                                    }
                                    #request{
                                        position:relative;
                                        
                                        margin-right:10px;
                                        margin-left:10px;
                                        font-weight:bold;
                                    }
                                    .scrollToDown{
                                        text-align: center;
                                        color: #fff;
                                        font-size:21px;
                                        font-weight:bold;
                                        position: fixed;
                                        padding-top:10px;
                                        bottom:60px;
                                        right:5px;
                                        width: 50px;
                                        height: 50px;
                                        cursor: pointer;
                                        background-color: rgba(0,0,0,.4);
                                        z-index: 1005;
                                        -webkit-backface-visibility: hidden;
                                        -webkit-transform: translateZ(0);
                                        -webkit-transition: all .2s ease-in-out;
                                        -o-transition: all .2s ease-in-out;
                                        transition: all .2s ease-in-out;
                                    }


                                
                </style>
	</head>

	
	<body class="no-trans  ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper ">
                   
			<!-- Offcanvas side start -->
                       
			<!-- offcanvas side end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">
                            <?php include 'includes/header.php';?>
                            <div class="container">
                            <div class="banner dark-translucent-bg" style="background-image:url('images/background-img-7.jpg'); background-position: 50% 32%;">
				<div class="container">
					<div class="row">
						<div class="col-md-8 text-center col-md-offset-2 pv-20">
							<h2 class="title object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100">Vision</h2>
							<div class="separator object-non-visible mt-10 animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100"></div>
                                                        <p class="text-center object-non-visible animated object-visible fadeIn" data-animation-effect="fadeIn" data-effect-delay="100" style="font-size: 19px;">We strive for a healthier Earth & Humans "inch by inch !"</p>
						</div>
					</div>
				</div>

			</div>
			<!-- banner end -->

			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">
							<div class="separator-2"></div>
							<div class="row">
								<div class="col-md-12">
									<div class="owl-item" >
                                                                                            <div class="overlay-container overlay-visible">
                                                                                                <center><img src="images/how_we_play.jpg" width="900"></center>
										</div>
                                                                                        </div>
                                                                        
								</div>
                                                            <p>&nbsp;</p>
							</div>
						</div>
						<!-- main end -->

					</div>
                                    
				</div>
                            <div class="section pv-40 parallax dark-translucent-bg background-img-3" style="box-shadow: rgba(0, 0, 0, 0.5) 0px 5px 10px inset; background-position: 50% 251px;">
				<div class="container">
					<div class="row">
                                            <div class="" style="margin-left: -30px;">
                                                    <div class="separator"></div>
                                                    <p style="text-align:center;font-family:century gothic;line-height:36px;font-size:32px;">How we work!</p>
                                                <center>
                                                    <div class="col-md-3"><img src="images/how_we_work_1.png" width="1148"></div>
                                                    <div class="col-md-3"><img src="images/how_we_work_2.png" width="1148"></div>
                                                    <div class="col-md-3"><img src="images/how_we_work_3.png" width="1148"></div>
                                                    <div class="col-md-3"><img src="images/how_we_work_4.png" width="1148"></div>
                                                </center>
						 <div class="separator"></div>
							
						</div>
					</div>
				</div>
			</div>

				<!-- section start -->
				<!-- ================ -->
				<div class="light-gray-bg pv-20 section mt-20">
					<div class="container">
                                                <div class="row" style="margin-left:50px; " id="team_mate">
                                                    <h4 class="mb-20" style="margin-left:15px; ">The <strong> Wolfpack</strong></h4>
							<div class="col-sm-6 col-md-3">
								<div class="image-box team-member style-2 shadow bordered mb-20 text-center">
								<div class="overlay-container overlay-visible" style="margin-top: 10px;">
                                                                    <center><img src="images/team-member-1.png" ></center>
									</div>
									<div class="body">
										<h3 class="margin-clear">Vivek Pal</h3>
                                                                                <i class="fa fa-phone pr-10 text-default"></i>
										<small>9742022399</small>
										<div class="separator mt-10"></div>
										<ul class="social-links circle animated-effect-1 colored">
                            <!--<li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>-->
                            <li class="instagram"><a target="_blank" href="http://www.instagram.com/"><i class="fa fa-instagram"></i></a></li>
                            <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                                
                        </ul>
									</div>
								</div>
							</div>
                                                    <div class="col-sm-1 col-md-1"></div>
							<div class="col-sm-6 col-md-3">
								<div class="image-box team-member style-2 shadow bordered mb-20 text-center">
								<div class="overlay-container overlay-visible" style="margin-top: 10px;">
                                                                            <center><img src="images/team-member-2.png" ></center>
									</div>
									<div class="body">
										<h3 class="margin-clear">Dhanashekar</h3>
                                                                                <i class="fa fa-phone pr-10 text-default"></i>
										<small>8105922266</small>
										<div class="separator mt-10"></div>
										<ul class="social-links circle animated-effect-1 colored">
                            <li class="instagram"><a target="_blank" href="http://www.instagram.com/"><i class="fa fa-instagram"></i></a></li>
                            <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                                
                        </ul>
									</div>
								</div>
							</div>
                                                    <div class="col-sm-1 col-md-1"></div>
							<div class="col-sm-6 col-md-3">
								<div class="image-box team-member style-2 shadow bordered mb-20 text-center">
                                                                    <div class="overlay-container overlay-visible" style="margin-top: 10px;">
                                                                            <center><img src="images/team-member-3.png" ></center>
									</div>
									<div class="body">
										<h3 class="margin-clear">Vinod Sagar</h3>
                                                                                <i class="fa fa-phone pr-10 text-default"></i>
										<small>9008688882</small>
										<div class="separator mt-10"></div>
										<ul class="social-links circle animated-effect-1 colored">
                            <li class="instagram"><a target="_blank" href="http://www.instagram.com/"><i class="fa fa-instagram"></i></a></li>
                            <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                                
                        </ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- section end -->
				
				<!-- section start -->
				<!-- ================ -->
				
				<!-- section end -->
				
			</section>
                        </div>
<?php include 'includes/footer.php'; ?>
                        
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<?php include 'includes/jsfiles.php'; ?>
                <script type="text/javascript" src="ajax/QuickQuery.js"></script>
                <?php include 'includes/support.php'; ?>
 
        
        
        <script>
            var check = true;
            $("body").click(function(){
                  if(!check){
                    closeNav(); 
                }
                   
                  
               });
            var plan_for_yo={};
            $(document).ready(function() {
               
               setReset();
            });
            function setReset(){
                plan_for_yo={};
                $("#steps").html('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-user pr-10" ></i><b id="title_list_tab">Select Your Gender</b></a></li>');
                $("#htab1").find("div:first-child").html('<div class="col-md-6 col-xs-6"><center><img onclick="setMale1();" src="images/icon/b-nutirifed-1.png" id="stepImg"/></center><p style="font-family:century gothic;text-align:center;margin-top:10px;font-weight:bold;" >Male</p></div><div class="col-md-6 col-xs-6"><center><img onclick="setFeMale1();" src="images/icon/b-nutirifed-2.png" id="stepImg"/></center><p style="font-family:century gothic;text-align:center;margin-top:10px;font-weight:bold;">Female</p> </div>');
                $("#letsgo").hide();    
            }
            function setMale1(){
                plan_for_yo['gender']="male";
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-male pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Select Your Goal");
                $("#htab1").find("div:first-child").html('<div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_loss();" src="images/icon/b-nutirifed-5.png" id="stepImg_2"/></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_mgmt();" src="images/icon/b-nutirifed-7.png" id="stepImg_2"/></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_gain();" src="images/icon/b-nutirifed-9.png" id="stepImg_2"/></center></div>');
            }
             function setFeMale1(){
                plan_for_yo['gender']="Female";
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-female pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Select Your Goal");
                $("#htab1").find("div:first-child").html('<div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_loss();" src="images/icon/b-nutirifed-3.png" id="stepImg_2" /></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_mgmt();" src="images/icon/b-nutirifed-6.png" id="stepImg_2" /></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_gain();" src="images/icon/b-nutirifed-8.png" id="stepImg_2" /></center></div>');
            }
            function setMale1_weight_loss(){
                plan_for_yo['goal']="weight_loss";
                setMale_activity();
            }
            function setMale1_weight_mgmt(){
                plan_for_yo['goal']="weight_mgmt";
                setMale_activity();
            }
            function setMale1_weight_gain(){
                plan_for_yo['goal']="weight_gain";
                setMale_activity();
            }
            function setMale_activity(){
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-bullseye pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Enrolled for any fitness activity ");
                $("#htab1").find("div:first-child").html('<div class="col-md-12 col-xs-8"> <center> <div class="form-group"> <label for="inputEmail3" class="control-label"></label> <br><div class="btn-group btn-toggle"> <button class="btn btn-xs btn-default-transparent" style="color:white" onclick="setOn()">Yes</button> <button class="btn btn-xs btn-default-transparent"style="color:white" onclick="setOff()">No</button> </div></div><div class="form-group" style="display:none"> <label for="inputEmail3" class="control-label">Please mention the Type of fitness in the below field</label> <input type="text" class="form-control" style="width:90%" id="id_on_off" placeholder="Gym, yoga”" disabled="true"> </div><div class="form-group"> <button href="#" class=" btn btn-lg btn-gray-transparent " id="id_next" onclick="setContactForm()" style="display:none;">Next</button> </div><label class="control-label" id="error_msg_id"></label> </center></div>');
            }
            var toogle=false;
            function setOn(){
                toogle=true;
                $("#id_on_off").parent().css({'display':'block'})
                $("#id_next").css({'display':'block'})
                $("#id_on_off").prop("disabled",false);
            }
            function setOff(){
                toogle=false;
                $("#id_on_off").parent().css({'display':'none'})
                $("#id_on_off").prop("disabled",true);
                plan_for_yo['fitness_activity']="No";
                setContactForm();
            }
            plan_for_yo['fitness_activity']="No";
            function setContactForm(){
                if(toogle){
                   if($("#id_on_off").val()!="No"){
                        plan_for_yo['fitness_activity']=$("#id_on_off").val();
                   }else{
                       $("#error_msg_id").html("Please mention the Type of fitness .").css({'color':'red'});
                       return false;
                   } 
                }
//                alert(JSON.stringify(plan_for_yo))
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-check pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Contact ");
                $("#htab1").find("div:first-child").html($('<div class="col-md-12"> <form style="margin-right: 19px;" class="form-horizontal" role="form" id="step_details"> <center> <h3>About You</h3></center> <div class="form-group"></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Name</label> <div class="col-sm-9"> <input type="text" class="form-control" id="name" name="name" placeholder="Your Name *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Email</label> <div class="col-sm-9"> <input type="email" id="email" name="email" class="form-control" placeholder="Email *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Contact No</label> <div class="col-sm-9"> <input type="text" id="contact" name="contact" class="form-control" placeholder="Contact *"> </div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Location</label> <div class="col-sm-9"> <input type="text" class="form-control" id="location" name="location" placeholder="Location eg. Koramangala *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Date of Birth</label> <div class="col-sm-9"> <input type="date" class="form-control" id="dob" name="dob"> </div></div><div class="form-group"> <center> <p id="errorMsg" style="color:red;font-family:verdana;font-weight:bold"></p></center> </div></form></div>'));
                $("#letsgo").show();
            }
            if(screen_width<768){
                  $("#team_mate").css({'margin-left':''})
              }else{
                  $("#team_mate").css({'margin-left':'50px'});
              }
            $(window).resize(function() {
              screen_width = $(window).width();
              if(screen_width<768){
                  $("#team_mate").css({'margin-left':''})
              }else{
                  $("#team_mate").css({'margin-left':'50px'});
              }
          });
        </script>
        <script type="text/javascript" src="ajax/Step.js"></script>
        
        
</html>


