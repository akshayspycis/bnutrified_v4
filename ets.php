
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->

	
<!-- Mirrored from htmlcoder.me/preview/the_project/v.1.0/template/components-animations.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jul 2015 17:58:19 GMT -->
<head>
		<meta charset="utf-8">
		<title>The Project | Animations</title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<link href="bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		
		<link href="css/animations.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
		<link href="plugins/owl-carousel/owl.transitions.css" rel="stylesheet">
		<link href="plugins/hover/hover-min.css" rel="stylesheet">		
		
		
		
		<!-- the project core CSS file -->
		<link href="css/style.css" rel="stylesheet" >

		
		

		<!-- Style Switcher Styles (Remove these two lines) -->
		<link href="#" data-style="styles" rel="stylesheet">
		<link href="style-switcher/style-switcher.css" rel="stylesheet">

		<!-- Custom css --> 
		<link href="css/custom.css" rel="stylesheet">
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans  ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!-- header-container start -->
			
			<!-- header-container end -->
		
			<!-- breadcrumb start -->
			<!-- ================ -->
			
			<!-- breadcrumb end -->

			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container">

				<div class="container">
					<div class="row">
						<div class="main col-md-8">
							<!-- page-title start -->
							<!-- ================ -->
							<h1 class="page-title">Animations</h1>
							<div class="separator-2"></div>
							<div class="row">
								<div class="col-md-3 col-sm-6 text-center mb-20">
									<div class="icon default-bg circle large gray-bg object-non-visible" data-animation-effect="zoomIn" data-effect-delay="1400"><i class="fa fa-file"></i></div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>
			
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="plugins/jquery.min.js"></script>
		<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="plugins/modernizr.js"></script>

		
		
		

		
		

		
		
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="plugins/jquery.validate.js"></script>

		
		

		
		

		
		
		

		
		

		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="plugins/owl-carousel/owl.carousel.js"></script>
		
		
		

		
		

		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="plugins/SmoothScroll.js"></script>

		<script type="text/javascript" src="js/template.js"></script>
		<script type="text/javascript" src="js/custom.js"></script>
		
		
		<!-- Color Switcher End -->
	</body>

<!-- Mirrored from htmlcoder.me/preview/the_project/v.1.0/template/components-animations.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jul 2015 17:58:19 GMT -->
</html>
