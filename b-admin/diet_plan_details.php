<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Diet Plan</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
          pro={}
          category={}
          var cat_id;
          $(document).ready(function(){
                  $.ajax({
                        type:"post",
                        url:"../server/controller/SelDietCategoryDetails.php",
                        success: function(data) { 
                            var duce = jQuery.parseJSON(data); //here data is passed to js object
                        category={}
                        $.each(duce, function (index, article) {
                        category[article.diet_category_details_id]=article;    
                            var a=$("<option></option>").append(article.diet_category).attr({'value':article.diet_category_details_id});
                            $("#search_category_id").find('#searchcategory_id').append(a);                    
                            a=$("<option></option>").append(article.diet_category).attr({'value':article.diet_category_details_id});
                            $("#insDietPlanDetails").find('#diet_category_details_id').append(a);                    
                        });
                            $("#search_category_id").find('#searchcategory_id').trigger("change");
                        }
                     });
                     $("#search_category_id").find('#searchcategory_id').change(function () {
                        var id = $( this ).val();
                        onlad(id)
                     }); 
                }); 
             
            
        </script>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Diet Plan</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
                <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Diet Plan</h3>
                  <div class="pull-right">
                      <button type="button" class="btn btn-success" id="addcat">Add Plan</button>
                  </div>
                  <br>
                  <br>
                  <div class="row">
                <div class="col-md-3" id="search_category_id">
        <div class="form-group" id="category">
                    <label>Select Category</label>
                    <select class="form-control select2" id="searchcategory_id" name="searchcategory_id">
                    </select>
                  </div><!-- /.form-group -->    
        </div>
                          
        </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                      <tr>
                        <th>S.N.</th>
                        <th>Plan</th>
                        <th>Price</th>
                        <th>Link</th>
                        <th colspan="2">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     
                      
                    </tbody>
                   
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert DietPlanDetails Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Insert Diet Plan </h4>
        </div>
        <form id ="insDietPlanDetails">    
        <div class="modal-body">
        <div class="row">
            <div class="col-md-6">
        <div class="form-group">
         <select class="form-control select2" id="diet_category_details_id" name="diet_category_details_id">
                            </select>
        </div>
        <!-- /.form-group -->
        </div><!-- /.col -->
        <div class="col-md-6">
        <div class="form-group">
         <input type="text"  name="plan" id="plan" value="" class="form-control"  placeholder="Enter plan">
        </div>
        <!-- /.form-group -->

        </div><!-- /.col -->
        
        <div class="col-md-6">
        <div class="form-group">
            <input type="number"  name="price" id="price" value="" class="form-control"  placeholder="Enter price">
        </div>
        <!-- /.form-group -->

        </div><!-- /.col -->
        <div class="col-md-6">
        <div class="form-group">
         <input type="text"  name="link" id="link" value="" class="form-control"  placeholder="Enter link">
        </div>
        <!-- /.form-group -->

        </div><!-- /.col -->
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert DietPlanDetails Insert Modal End-->
       <!--Insert DietPlanDetails Edit Modal Start-->
<div class="modal fade" id="edit1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Diet Plan </h4>
</div>
<form id ="editDietPlanDetails">    
<div class="modal-body">
<div class="row">

<div class="col-md-6">
<div class="form-group">
    <input type="hidden"  name="diet_plan_details_id" id="diet_plan_details_id" value="" class="form-control"/>
    <input type="text"  name="editcate_plan" id="editcate_plan" value="" class="form-control"  placeholder="Enter Plan">
</div>
</div>
<div class="col-md-6">
<div class="form-group">
    <input type="number"  name="editcate_price" id="editcate_price" value="" class="form-control"  placeholder="Enter Price">
</div>
</div><!-- /.col -->
<div class="col-md-6">
<div class="form-group">
<input type="text"  name="editcate_link" id="editcate_link" value="" class="form-control"  placeholder="Enter Link">
</div>
</div><!-- /.col -->
</div><!-- /.box -->

<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert DietPlanDetails Edit Modal End-->
         <!--Insert DietPlanDetails Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Plan</h4>
        </div>
        <form id ="deleteDietPlanDetails">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Diet Plan Details?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert DietPlanDetails Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
        $("#addcat").click(function(){
            $("#myModal").modal('show');
            }); 
            $("#myModal").on('shown.bs.modal', function(){
                            $('#insDietPlanDetails').off("submit");
                            $('#insDietPlanDetails').submit(function() {
                            
                            if(this.plan.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Plan ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#plan').focus();
                                $('#insDietPlanDetails').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }else if(this.price.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Price ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#price').focus();
                                $('#insDietPlanDetails').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                     }else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/InsDietPlanDetails.php",
                        data:$('#insDietPlanDetails').serialize(),
                        success: function(data){ 
                        $("#search_category_id").find('#searchcategory_id').trigger("change");
                        $('#insDietPlanDetails').each(function(){
                                this.reset();
                                return false;
                             });
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
    cate={}
         function onlad(diet_category_details_id){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelDietPlanDetails.php",
                    data:{'diet_category_details_id':diet_category_details_id},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            cate[article.diet_plan_details_id]=article;
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.plan))
                                .append($('<td/>').html(article.price))
                                .append($('<td/>').html(article.link))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.diet_plan_details_id+" data-DietPlanDetails="+article.category+" id=\"edit\" data-toggle=\"modal\" data-target=\"#edit1\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-id="+article.diet_plan_details_id+" data-DietPlanDetails="+article.category+"  id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                            );
                        });
                    }
                });
         }
  $(document).on("click", "#edit", function () {
                       var diet_plan_details_id = $(this).attr('data-id');
                       $("#edit1").on('shown.bs.modal', function(){
                        $("#edit1").find("#diet_plan_details_id").val(diet_plan_details_id);  
                        $("#edit1").find("#editcate_plan").val(cate[diet_plan_details_id]["plan"]);          
                        $("#edit1").find("#editcate_price").val(cate[diet_plan_details_id]["price"]);          
                        $("#edit1").find("#editcate_link").val(cate[diet_plan_details_id]["link"]);          
                            $('#editDietPlanDetails').off("submit");
                            $('#editDietPlanDetails').submit(function() {
                            if(this.editcate_plan.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Plan").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editcate_plan').focus();
                           }else if(this.editcate_price.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Price").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editcate_price').focus();
                           }else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/UpdDietPlanDetails.php",
                        data:$('#editDietPlanDetails').serialize(),
                        success: function(data){ 
                             location.reload(true);
                                } 
                            });
                    }
                   return false;
    });
   
     var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#edit1').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});

</script>

<script type="text/javascript" language="javascript">
        $(document).on("click", "#delete", function () {
                 var c_Id = $(this).data('id');
                 var plan = cate[c_Id]["plan"];;
                 $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#catname").text(plan); 
                    $('#deleteDietPlanDetails').off("submit");
                    $('#deleteDietPlanDetails').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelDietPlanDetails.php",
                            data:"diet_plan_details_id="+c_Id,
                            success: function(data){ 
                            $("#search_category_id").find('#searchcategory_id').trigger("change");
                            $('#deleteDietPlanDetails').each(function(){
                                this.reset();
                                $('#delete1').modal('hide');
                                    return false;
                                    });
                                } 
                  });
                    return false;
    });
    
    });
   
});

</script>
</html>

