<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Student Details</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
   
        <script type="text/javascript">
              cate={}
         $(document).ready(function() {
              onlad($("#school_name").val(),$("#class_name").val());
              $("#show_student_details").click(function () {
                onlad($("#school_name").val(),$("#class_name").val())
              });
              $("#select_all").click(function () {
                    $("#data").find("tbody").find("tr td:nth-child(11)").find('input:checkbox').prop('checked',true); 
              });
              $("#send_message").click(function () {
                    $("#myModal").modal("show");
              });
              
         });  
        
        </script> 
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
 <body class="wysihtml5-supported skin-yellow sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Student Details</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Student Details</h3>
                  <div class="pull-right">
<!--                      <button type="button" class="btn btn-success">Add Student</button>-->
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                    <div class="row">
                   <div class="col-md-6">
                     <div class="form-group">
                         <label for="sel1">Select School Name</label>
                          <select class="form-control" name="school_name" id="school_name">
                          <option value="Modern Convent School Mandideep">Modern Convent School Mandideep</option>
                          <option value="Modern Convent School Obedullaganj">Modern Convent School Obedullaganj</option>
                          <option value="Modern English Medium Higher Secondary School Tamot">Modern English Medium Higher Secondary School Tamot</option>
                        </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                          <div class="form-group">
                            <label for="student name">Admission In Class </label>
                            <select class="form-control" name="class_name" id="class_name">
                            <option value="Pre School">Pre School</option>
                            <option value="1st">1st</option>
                            <option value="2nd">2nd</option>
                            <option value="3rd">3rd</option>
                            <option value="4th">4th</option>
                            <option value="5th">5th</option>
                            <option value="6th">6th</option>
                            <option value="7th">7th</option>
                            <option value="8th">8th</option>
                            <option value="9th">9th</option>
                            <option value="10th">10th</option>
                            <option value="11th">11th</option>
                            <option value="11th">12th</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <button type="button" class="btn btn-success" id="show_student_details" style="margin-top: 26px;">View</button>
                    </div>
                    </div>
                  <div class="row">
                      <div class="col-md-10">
                          <div class="pull-right">
                            <button type="button" class="btn btn-success" id="send_message" style="margin-bottom: 26px;">Send Message</button>
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="pull-left" >
                            <button type="button" class="btn btn-success" id="select_all" style="margin-bottom: 26px;">Select All</button>
                          </div>
                      </div>
                        
                    </div>
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Gender</th>
                        <th>Dob</th>
                        <th>Contact No</th>
                        <th>Email</th>
                        <th>Fathers Name</th>
                        <th>fathers Contact</th>
                        <th>fathers Email</th>
                        <th colspan="3" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
 <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send Message</h4>
        </div>
        <form id ="insCategory">    
        <div class="modal-body">
        <div class="row">
        
        <div class="col-md-6">
        <div class="form-group">
            <label>Contact List</label><br>
            <textarea name="contact_list" id="contact_list" rows="10" cols="40"></textarea>
            
        </div>
        </div><!-- /.col -->
        <div class="col-md-6">
        <div class="form-group">
               <label>Message</label><br>
               <textarea name="message_content" id="message_content" rows="10" cols="40"></textarea>
        </div>
        </div><!-- /.col -->
       
        </div><!-- /.box -->


        <div class="modal-footer">
            <div class="validate pull-left" style="font-weight:bold;"></span></div>
        <button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
      <!--Insert Category Insert Modal End-->
       <!--Insert Category Edit Modal Start-->
<div class="modal fade" id="edit1" role="dialog">
<div class="modal-dialog">

<!-- Modal content-->
<div class="modal-content">

<div class="modal-header">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h4 class="modal-title">Edit Category </h4>
</div>
<form id ="editCategory">    
<div class="modal-body">
<div class="row">

<div class="col-md-6">
<div class="form-group">
<input type="hidden"  name="blog_category_details_id" id="blog_category_details_id" value="" class="form-control"/>
<input type="text"  name="editcate" id="editcate" value="" class="form-control"  placeholder="Enter Category Name">
</div>
<!-- /.form-group -->

</div><!-- /.col -->
</div><!-- /.box -->


<div class="modal-footer">
<div class="validate pull-left" style="font-weight:bold;"></span></div>
<button type="submit" id ="update" class="btn btn-primary">Update</button>
<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

</div>
</div>
</form>
</div>
</div>
</div>
        <!--Insert Category Edit Modal End-->
         <!--Insert Category Delete Modal Start-->
 <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Category </h4>
        </div>
        <form id ="deleteCategory">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="blog_category_details_id" id="blog_category_details_id" value="" class="form-control"/>
        <input type="hidden"  name="Category" id="dCategory" value="" class="form-control"/>
        <p>Sure to want to delete "<span style ="color:red" id ="catname"></span>" Category?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Delete</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div>
          <!--Insert Category Delete Modal End--> 
  </body>
  <script type="text/javascript" language="javascript">
      $(document).ready(function(){
        $("#addcat").click(function(){
            $("#myModal").modal('show');
            }); 
            
            $("#myModal").on('shown.bs.modal', function(){
                var contact_lsit="";
                $("#data").find("tbody").find("tr td:nth-child(11)").find('input:checkbox').each(function (){
                    if(contact_lsit!="")contact_lsit=contact_lsit+",";
                    contact_lsit=contact_lsit+$(this).val();
                })
                $("#contact_list").val(contact_lsit)
                            $('#insCategory').off("submit");
                            $('#insCategory').submit(function() {
                            
                            if(this.category.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid category ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#category').focus();
                                $('#insCategory').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/InsAdmissions.php",
                        data:$('#insCategory').serialize(),
                        success: function(data){ 
                        onlad();
                        $('#insCategory').each(function(){
                                this.reset();
                                return false;
                             });
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    });
   
    var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#myModal').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
         }); 
    
         function onlad(school_name,class_name){
                     $.ajax({
                    type:"post",
                    url:"../server/controller/SelAdmissions.php",
                    data:{'school_name':school_name,'class_name':class_name},
                    success: function(data) {
                        var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                           cate[article.admissions_id]= article;
                           var img =$("<img>").attr({'src':'../'+article.image}).addClass("img-circle").css({'width':'100%','max-width': '45px','height':'auto'})
                           $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(img))
                                .append($('<td/>').html(article.first_name+" "+article.middle_name+" "+article.last_name))
                                .append($('<td/>').html(article.gender))
                                .append($('<td/>').html(article.dob))
                                .append($('<td/>').html(article.contact))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.fathers_name))
                                .append($('<td/>').html(article.fathers_contact))
                                .append($('<td/>').html(article.fathers_emailid))
                                .append($('<td/>').html($("<input>").attr({'type':'checkbox','value':article.contact})))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-id="+article.blog_category_details_id+" data-Category="+article.category+" id=\"edit\" data-toggle=\"modal\" data-target=\"#edit1\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-id="+article.blog_category_details_id+" data-Category="+article.category+"  id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                                
                            );
                        });
                    }
                });
         }
  $(document).on("click", "#edit", function () {
                       var blog_category_details_id = $(this).attr('data-id');
                       $("#edit1").on('shown.bs.modal', function(){
                        $("#edit1").find("#blog_category_details_id").val(blog_category_details_id);  
                        $("#edit1").find("#editcate").val(cate[blog_category_details_id]["category"]);          
                            $('#editCategory').off("submit");
                            $('#editCategory').submit(function() {
//                            var catfilter = /^[A-Za-z\d\s]+$/;
                            if(this.editcate.value == ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid category ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#editcate').focus();
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        
                        type:"post",
                        url:"../server/controller/UpdAdmissions.php",
                        data:$('#editCategory').serialize(),
                        success: function(data){ 

                             location.reload(true);
                                } 
                            });
                    }
                   return false;
    });
   
     var modal = this;
            var hash = modal.id;
            window.location.hash = hash;
            window.onhashchange = function() {
                    if (!location.hash){
                            $(modal).modal('hide');
                    }
            }
      });  
$('#edit1').on('hide.bs.modal', function() {
    	location.reload(true);
        
});
});

</script>

<script type="text/javascript" language="javascript">
        $(document).on("click", "#delete", function () {
                 var c_Id = $(this).data('id');
                 var Category = cate[c_Id]["category"];;
                 $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#blog_category_details_id").val(c_Id);        
                    $("#delete1").find("#catname").text(Category); 
                    $('#deleteCategory').off("submit");
                    $('#deleteCategory').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DelAdmissions.php",
                            data:"blog_category_details_id="+c_Id,
                            success: function(data){ 
                            $('#deleteCategory').each(function(){
                                this.reset();
                                onlad();
                                $('#delete1').modal('hide');
                            
                                    return false;
                                    });
                                } 
                  });
                    return false;
    });
    
    });
   
});

</script>
</html>

