<?php include './includes/check_session.php';?>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Queries</title>
    
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
     
     <script type="text/javascript">
          queries={}
          $(document).ready(function(){
              onlad();
            });  
      </script> 
    <style>
          #data tr th{
                background:green;
                color:white; 
            }
        .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
    </style>
  </head>
  <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
  <body class="skin-yellow sidebar-mini sidebar-collapse">
    <!-- Site wrapper -->
    <div class="wrapper">

   <?php include 'includes/header.php';?>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Admin panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><i class="fa fa-dashboard"></i> Home</li>
            <li class="active">Queries</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
           
          <div class="row">
            <div class="col-xs-12">
           

              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Queries</h3>
                  <div class="pull-right">
                      <!--<button type="button" class="btn btn-success" id="addFeedback">Add Feedback</button>-->
                  </div>
                </div><!-- /.box-header -->
                
                <div class="box-body">
                  <div class="table-responsive"> 
                   <table id="data" class="table table-bordered table-hover ">
                    <thead>
                       <tr>
                        <th width="5%">S.N.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>School Name</th>
                        <th>Date</th>
                       <th colspan="2" style="text-align:center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                    <tfoot>
                      
                    </tfoot>
                  </table>
                  </div>         
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section>
      </div><!-- /.content-wrapper -->

     <?php // include 'includes/footer.php';?>

     
    </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
    <!--Insert Category Insert Modal Start-->
     

      <div class="modal fade" id="delete1" role="dialog">
        <div class="modal-dialog">
                
        <!-- Modal content-->
        <div class="modal-content">
             
         <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Delete Feedbacks </h4>
        </div>
        <form id ="deleteFeedback">    
        <div class="modal-body">
        <div class="row">
        <div class="col-md-6">
        <input type="hidden"  name="sid" id="sid" value="" class="form-control"/>
        <p id ="msg">Sure to want to delete ?</p>
         </div>
        </div>
        <div class="modal-footer">
        <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        
        </div>
        </div>
       </form>
        </div>
        </div>
       </div> 
  </body>

    

<script type="text/javascript" language="javascript">
      /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
                 var quick_query_id = $(this).data('quick_query_id');
               
                    $("#delete1").on('shown.bs.modal', function(){
                    $("#delete1").find("#quick_query_id").val(quick_query_id);
                       $('#deleteFeedback').off("submit");
                        $('#deleteFeedback').submit(function() {
                         $.ajax({
                            type:"post",
                            url:"../server/controller/DellQuickQuery.php",
                            data:"quick_query_id="+quick_query_id,
                            success: function(data){ 
                               
                        $('#deleteFeedback').each(function(){
                                this.reset();
                                onlad();
               $('#delete1').modal('hide');
                        return false;
                     });
             } 
                  });
                    return false;
    });
    
    });
   
});
  /* Code for Product Delete end */  
     
//...........................................................................................................

function onlad(){ 
//              alert("success");
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    data:{'school_id':""},
                    url:"../server/controller/SelQuickQuery.php",
                    success: function(data) {
//                        alert(data.trim());
                      var duce = jQuery.parseJSON(data);
                        $("#data tr:has(td)").remove();
                        $.each(duce, function (index, article) {
                            queries[article.quick_query_id]={};
                            queries[article.quick_query_id]["name"]=article.name;
                            queries[article.quick_query_id]["email"]=article.email;
                            queries[article.quick_query_id]["contact"]=article.contact;
                            queries[article.quick_query_id]["subject"]=article.subject;
                            queries[article.quick_query_id]["message"]=article.message;
                            queries[article.quick_query_id]["school_name"]=article.school_name;
                            queries[article.quick_query_id]["date"]=article.date;
                          $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.name))
                                .append($('<td/>').html(article.email))
                                .append($('<td/>').html(article.contact))
                                .append($('<td/>').html(article.subject))
                                .append($('<td/>').html(article.message))
                                .append($('<td/>').html(article.school_name))
                                .append($('<td/>').html(article.date))
//                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-quick_query_id="+article.quick_query_id+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editQueries\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-quick_query_id="+article.quick_query_id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                             );
                        });
                    }
                });
                }
</script>


</html>


