  <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img src="style/images/user.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p><?php echo "Welcome"." ".$_SESSION['user_name']; ?></p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
          </div>

          <ul class="sidebar-menu">
            <li class="header">Main  Navigation</li>
            <li>
                <a href="home.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Dashboard</span>
              </a>
            </li>
            <li>
                <a href="food_category_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Food Category</span>
              </a>
            </li>
            <li>
                <a href="food_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Food Details</span>
              </a>
            </li>
            <li>
                <a href="diet_category_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Diet Category</span>
              </a>
            </li>
            <li>
                <a href="diet_plan_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Diet Plan</span>
              </a>
            </li>
            <li>
                <a href="quote-request.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Quote Request</span>
              </a>
            </li>
            <li class="treeview">
                    <a href="banners.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Banners</span>
              </a>
            </li>
            <li>
                <a href="feedback.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Feedback </span>
              </a>
            </li> 
            <li>
                <a href="services.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Services</span>
              </a>
            </li> 
            <li>
                <a href="blog_category.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Blog Category</span>
              </a>
            </li> 
            <li>
                <a href="blog_details.php">
                <i class="fa fa-angle-double-right"></i>
                <span>Blog</span>
              </a>
            </li> 
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
            <li>&nbsp;</li>
         </ul>
        </section>
        <!-- /.sidebar -->
      </aside>