<?php include './includes/check_session.php';?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Faq</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.4 -->
    <?php include 'includes/links.php';?>
        
        <script type="text/javascript">
            faq={};
            $(document).ready(function(){
                onlad();
                $.ajax({
                              type:"post",
                               url:"../server/controller/selfaqCategory.php",
                                 success: function(data) { 
                                 var duce = jQuery.parseJSON(data); //here data is passed to js object
                                  $.each(duce, function (index, article) {
                                  var a=$("<option></option>").append(article.fc_name).attr({'value':article.fc_id});
                                         $('#updateform').find('#fc_id').append(a);                    
                                    });
                                    }
                                    });
            });  
        </script>
        <style>
            .uploadArea{ min-height:180px; height:auto; border:1px dotted #ccc; padding:10px; cursor:move; margin-bottom:10px; position:relative;}
            h1, h5{ padding:0px; margin:0px; }
            h1.title{ font-family:'Boogaloo', cursive; padding:10px; }
            .uploadArea h1{ color:#ccc; width:100%; z-index:0; text-align:center; vertical-align:middle; position:absolute; top:25px;}
            .dfiles{ clear:both; border:1px solid #ccc; background-color:#E4E4E4; padding:3px;  position:relative; height:25px; margin:3px; z-index:1; width:97%; opacity:0.6; cursor:default;}
        </style>
    </head>
    <!-- ADD THE CLASS sidedar-collapse TO HIDE THE SIDEBAR PRIOR TO LOADING THE SITE -->
    <body class="skin-yellow sidebar-mini sidebar-collapse">
        <!-- Site wrapper -->
        <div class="wrapper">
            
   <?php include 'includes/header.php';?>
            
            <!-- =============================================== -->
            
            <!-- Left side column. contains the sidebar -->
   <?php include 'includes/sidepanel.php';?>
            
            <!-- =============================================== -->
            
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Admin panel</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i> Home</li>
                        <li class="active">News & Updates</li>
                    </ol>
                </section>
                
                <!-- Main content -->
                <section class="content">
                    
                    <div class="row">
                        <div class="col-xs-12">
                            
                            
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Faq's</h3>
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-success" id="addFaq">Add Faq</button>
                                    </div>
                                </div><!-- /.box-header -->
                                
                                <div class="box-body">
                                    <div class="table-responsive"> 
                                        <table id="data" class="table table-bordered table-hover ">
                                            <thead>
                                                <tr>
                                                    <th width="5%">S.N.</th>
                                                    <th>Question</th>
                                                    <th>Answer</th>
                                                    <th>category</th>
                                                    <th>posted On</th>
                                                    <th colspan="2" style="text-align:center">Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                
                                            </tbody>
                                            <tfoot>
                                                
                                            </tfoot>
                                        </table>
                                    </div>         
                                    
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </section>
            </div><!-- /.content-wrapper -->
            
     <?php // include 'includes/footer.php';?>
            
            
        </div><!-- ./wrapper -->
    <?php include 'includes/jslinks.php';?>
        <!--Insert Category Insert Modal Start-->
        <div class="modal fade" id="insFaq" role="dialog">
            <div class="modal-dialog">
                
                <!-- Modal content-->
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Insert Faq's </h4>
                    </div>
                    <form id ="insertfaq" enctype="multipart/form-data">    
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="sel1" >Categories</label>
                                                        <select class="form-control select2" id="fc_id" name="fc_id">
                                                            <option value="">Please Select Category</option>
                                                        </select>
                                                     </div>
                                                    <div class="form-group">
                                                        <label for="question">Question</label>
                                                        <input type="text" class="form-control"  id="f_question" name="f_question" placeholder="Question here">
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label>Answer</label>
                                                        <textarea class="form-control" rows="5" id="f_answer" name="f_answer" style="resize:none" placeholder="Content here"></textarea>
                                                    </div>
                                                </div>
                                            </div><!-- /.row -->
                                        </div><!-- /.box-body -->
                                        
                                        
                                    </div><!-- /.box -->
                                </div><!-- /.col -->
                                
                                
                            </div><!-- /.box -->
                            
                            
                            <div class="modal-footer">
                                <div class="validate pull-left" style="font-weight:bold;"></span></div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editfaq" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Faq </h4>
                    </div>
                    <form id ="updateform" enctype="multipart/form-data">    
                         <div class="modal-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="box">
                                        <div class="box-body">
                                            <div class="row">
                                                <div class='col-md-12'>
                                                    <div class="form-group">
                                                        <label for="question">Question</label>
                                                        <input type="text" class="form-control"  id="f_question" name="f_question" placeholder="Question here">
                                                    </div>
                                                    <!-- /.form-group -->
                                                    <div class="form-group">
                                                        <label>Answer</label>
                                                        <textarea class="form-control" rows="5" id="f_answer" name="f_answer" style="resize:none" placeholder="Content here"></textarea>
                                                    </div>
                                                </div>
                                            </div><!-- /.row -->
                                        </div><!-- /.box-body -->
                                        
                                        
                                    </div><!-- /.box -->
                                </div><!-- /.col -->
                            </div><!-- /.box -->
                            <div class="modal-footer">
                                <div class="validate pull-left" style="font-weight:bold;"></span></div>
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade" id="delete1" role="dialog">
            <div class="modal-dialog">
                
                <!-- Modal content-->
                <div class="modal-content">
                    
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Delete News</h4>
                    </div>
                    <form id ="deleteFaq">    
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="hidden"  name="f_id" id="f_id" value="" class="form-control"/>
                                    <p id ="msg">Sure to want to delete ?</p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" id="delete2" class="btn btn-danger">Ok</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div> 
    </body>
    <script type="text/javascript" language="javascript">
         $(document).ready(function(){
            $("#addFaq").click(function(){
            $("#insFaq").modal('show'); }); 
            $("#insFaq").on('shown.bs.modal', function(){
                           $.ajax({
                                    type:"post",
                                    url:"../server/controller/selfaqCategory.php",
                                    success: function(data) { 
                                    var duce = jQuery.parseJSON(data); //here data is passed to js object
                                    $.each(duce, function (index, article) {
                                        var a=$("<option></option>").append(article.fc_name).attr({'value':article.fc_id});
                                         $('#insertfaq').find('#fc_id').append(a);                    
                                    });
                                    }
                                    }); 
                            $('#insertfaq').submit(function() {
                            if(this.fc_id.value === ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please Select Category ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#fc_id').focus();
                                $('#insertfaq').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }
                            else if(this.f_question.value === ""){
                                $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Question ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#f_question').focus();
                                $('#insertfaq').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }else if(this.f_answer.value === ""){
                              $(".validate").addClass("text-danger").fadeIn(100).text(" Please fill valid Answer ").prepend("<span class=\"glyphicon glyphicon-remove text-danger\">").fadeOut(1000);
                                $('#f_answer').focus();
                                $('#insertfaq').each(function(){
                                this.reset();
                                return false;
                             });
                             return false;  
                           }

                    else {
                        $(".validate").removeClass("text-danger").addClass("text-success").fadeIn(100).text("Ok").prepend("<span class=\"glyphicon glyphicon-ok text-success\">").fadeOut(1000);
                        $.ajax({
                        type:"post",
                        url:"../server/controller/insfaq.php",
                        data:$('#insertfaq').serialize(),
                        success: function(data){ 
                      
                        $('#insertfaq').each(function(){
                        this.reset();
                         location.reload(true);
                        return false;
                        });
                          return false;
                      } 
               });
                    return false; 
                    }
                   return false;
    }); });});
    </script>
    <script>
         $(document).on("click", "#edit", function () {
             
                var f_id = $(this).data('f_id');
                 $("#editfaq").on('shown.bs.modal', function(){
                                    $("#editfaq").find("#f_id").val(f_id);  
                                    $("#editfaq").find("#fc_id").val(faq[f_id]["f_cat"]);
                                    $("#editfaq").find("#f_question").val(faq[f_id]["f_question"]);  
                                    $("#editfaq").find("#f_answer").val(faq[f_id]["f_answer"]);  
                                    $('#updateform').submit(function() {
                        $.ajax({
                        type:"post",
                        url:"../server/controller/updateFaq.php",
                        data:$('#updateform').serialize(),
                        success: function(data){
                           onlad();
                          $('#updateform').each(function(){
                            this.reset();
                            $('#editfaq').modal('hide');
                            return false;
                                    });
                              } 
                            });
                   
                   return false;
    });
   
    });
});
    </script>
    <script type="text/javascript" language="javascript">
        /* Code for Product Delete start */  
        $(document).on("click", "#delete", function () {
            var f_id = $(this).data('f_id');
               
            $("#delete1").on('shown.bs.modal', function(){
                $("#delete1").find("#f_id").val(f_id);        
                $('#deleteFaq').submit(function() {
                    $.ajax({
                        type:"post",
                        url:"../server/controller/deleteFaq.php",
                        data:"f_id="+f_id,
                        success: function(data){ 
                              
                            $('#deleteFaq').each(function(){
                                this.reset();
                                onlad();
                                $('#delete1').modal('hide');
                                return false;
                            });
                        } 
                    });
                    return false;
                });
    
            });
   
        });
        /* Code for Product Delete end */  
     
        //...........................................................................................................

        function onlad(){
           
            /* Ajax call for Product Display*/
            $.ajax({
                type:"post",
                url:"../server/controller/selectfaq.php",
                success: function(data) {
                    
                    var duce = jQuery.parseJSON(data);
                    $("#data tr:has(td)").remove();
                    $.each(duce, function (index, article) {
                       
                        faq[article.f_id]={};
                        faq[article.f_id]["f_question"]=article.f_question;
                        faq[article.f_id]["f_answer"]=article.f_answer;
                        faq[article.f_id]["f_cat"]=article.f_cat;
                        faq[article.f_id]["f_date"]=article.f_date;
                        $("#data").append($('<tr/>')
                                .append($('<td/>').html((index+1)))
                                .append($('<td/>').html(article.f_question))
                                .append($('<td/>').html(article.f_answer))
                                .append($('<td/>').html(article.f_cat))
                                .append($('<td/>').html(article.f_date))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-success btn-xs\" data-f_id="+article.f_id+" id=\"edit\" data-toggle=\"modal\" data-target=\"#editfaq\")>&nbsp;&nbsp;Edit&nbsp;&nbsp;</button>"))
                                .append($('<td/>').html("<button type=\"button\" class=\"btn btn-danger btn-xs\"  data-f_id="+article.f_id+"   id =\"delete\" data-toggle=\"modal\" data-target=\"#delete1\"\")>Delete</button>"))
                                );
                    });
                }
            });
        }
    </script>
    
    
</html>

