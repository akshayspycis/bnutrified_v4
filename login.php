<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
		<title>Login into B-Nutrified</title>
		<meta name="description" content="">
		<meta name="author" content="">
                <link rel="shortcut icon" href="images/fevicon.png">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                 <?php include './includes/cssfiles.php';?> 
                <style>
                    .btn-default-transparent:active,.btn-default-transparent:focus{
                        background:none;
                            color: #777;
                    }
                    .tips {
                    display: inline-block;
                    position: relative;
                    width: auto;
                    min-width: 180px;
                    height: auto;
                    vertical-align: top;
                    margin-left: 35px;
                    margin-right: 35px;
                    margin-bottom: 50px;
                    }
                    .innerTip {
                    display: inline-block;
                    vertical-align: middle;
                    width: 100%;
                    margin-left:auto;
                    margin-right:auto;
                    text-align:center;
                    }
                    .innerTipTitle {
                    font-family:century gothic;
                    font-weight:bold;
                    
                    color: #007833;
                    font-size: 16px;
                    line-height: 20px;
                    text-align: center;
                    text-decoration: none;
                    vertical-align: top;
                    padding-top: 10px;
                    }
                    #steps li{
                        font-family:century gothic;
                        font-size:24px;
                        font-weight:bold;
                        
                    }
                    #steps li a{
                        font-family:century gothic;
                        font-size:16px;
                        font-weight:bold;
                        background:transparent;
                       
                       
                    }
                    #steps li a:active{
                        background:orangered;
                        color:white;
                         background:transparent;
                    }
                    #stepContent{
                        position:relative;
                        top:30px;
                        background:#e9ebee;
                    }
                    #stepImg{
                        cursor:pointer;
                    }
                    #stepImg_2{
                        cursor:pointer;
                        height: 200px;
                    }
                    #stepContentInner{
                        background:#e9ebee;
                    }
                                    #footer-top {
                                    background: #0d5995;
                                    min-height: 40px;
                                    border-bottom: 1px solid white;
                                    height: auto;
                                    color: white;
                                    margin-top: 1px;
                                    padding-top: 15px;
                                    box-shadow: inset 0 4px 10px rgba(0,0,0, 0.50);
                                    border-bottom: 1px solid white;
                                    }
                                    #menuBar{
                                        position:relative;
                                        float:right;
                                        top:3px;
                                        font-size:16px;
                                        color:black;
                                        cursor:pointer;
                                        
                                    }
                                    #menuBar:hover{
                                        color:#0d5995;
                                        
                                    }
                                    #request{
                                        position:relative;
                                        
                                        margin-right:10px;
                                        margin-left:10px;
                                        font-weight:bold;
                                    }
                                    .scrollToDown{
                                        text-align: center;
                                        color: #fff;
                                        font-size:21px;
                                        font-weight:bold;
                                        position: fixed;
                                        padding-top:10px;
                                        bottom:60px;
                                        right:5px;
                                        width: 50px;
                                        height: 50px;
                                        cursor: pointer;
                                        background-color: rgba(0,0,0,.4);
                                        z-index: 1005;
                                        -webkit-backface-visibility: hidden;
                                        -webkit-transform: translateZ(0);
                                        -webkit-transition: all .2s ease-in-out;
                                        -o-transition: all .2s ease-in-out;
                                        transition: all .2s ease-in-out;
                                    }


                                
                </style>
	</head>

	
	<body class="no-trans  ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
                   
			<!-- Offcanvas side start -->
                       
			<!-- offcanvas side end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">
                         <?php include 'includes/header.php';?>
			<div class="banner dark-translucent-bg" style="background-image:url('images/background-img-61.jpg'); background-position: 50% 32%;">
				<a href="#homeRow1" id="scrollDownButton"><div class="scrollToDown circle"  style="display: block;"><i class="icon-down-open-big"></i></div></a>
				<div class="container">
					<div class="row">
						<div class="col-md-8 text-center col-md-offset-2 pv-20">
							<h1 class="object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100"><strong>Login</strong></h1>
							<!--<div class="separator object-non-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>-->
							<!--<p class="text-center object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100">Deep down we all want to be fit and have a perfect physique! We just don't know where to start from, even if we do the brain just gives in to all the cravings of unhealthy and lazy. Different people around you suggest different, who knows how far they are true! That could just be something you would never like to do; moreover, it might just not work on you, you see! You are unique! All these constraints keep pushing your dream to be fit again. Worry not! We can create a solution for you, we understand you are unique! Here is how we can help you.</p>-->
						</div>
					</div>
				</div>
			</div>
                            <section class="main-container padding-bottom-clear">
				<div class="container">
					<div class="row">
						<!-- ================ -->
						<div class="main col-md-12">
							<div class="row">
								<div class="col-md-12">
									<div class="form-block center-block p-30 light-gray-bg border-clear">
                <h2 class="title">Login</h2>
                <form class="form-horizontal" id="userLogin">
                    <div class="form-group has-feedback">
                        <label for="inputUserName" class="col-sm-3 control-label">Your Email</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="email" name="email" placeholder="Your Email">
                            <i class="fa fa-user form-control-feedback"></i>
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label for="inputPassword" class="col-sm-3 control-label">Password</label>
                        <div class="col-sm-8">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                            <i class="fa fa-lock form-control-feedback"></i>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-8">
                            <div class="checkbox">
                                <label>
                                    <!--<input type="checkbox" > Remember me.-->
                                </label>
                            </div>											
                            <button type="submit" class="btn btn-group btn-default btn-animated">Log In <i class="fa fa-user"></i></button>
                            <button type="button" class="btn  btn-dark" data-dismiss="modal">Close</button>
                            <ul class="space-top">
                                <li><a href="#">Forgot your password?</a></li>
                            </ul>
<!--                            <span class="text-center text-muted">Login with</span>
                            <ul class="social-links colored circle clearfix">
                                <li class="facebook"><a target="_blank" href="http://www.facebook.com/"><i class="fa fa-facebook"></i></a></li>
                                <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                            </ul>-->
                        </div>
                    </div>
                </form>
                                                                    
                <div id="errorMsg_login" style="color:red;font-family:verdana;font-weight:bold"></div>
                <div id="successMsg_login" style="color:green;font-family:verdana;font-weight:bold"></div>
            </div>
                                                                        
								</div>
                                                            <p>&nbsp;</p>
							</div>
						</div>
						<!-- main end -->

					</div>
                                    
				</div>
                            

				<!-- section start -->
				<!-- ================ -->
				
				<!-- section end -->
				
				<!-- section start -->
				<!-- ================ -->
				
				<!-- section end -->
				
			</section>
			<!-- banner end -->

			<!-- main-container start -->
			<!-- ================ -->
			
<?php include 'includes/footer.php'; ?>
		</div>
                        
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<?php include 'includes/jsfiles.php'; ?>
                <script type="text/javascript" src="ajax/QuickQuery.js"></script>
                <?php include 'includes/support.php'; ?>
 
        
        
        
        <script type="text/javascript" src="ajax/UserLogin.js"></script>
        
        
</html>


