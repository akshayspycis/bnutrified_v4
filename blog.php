<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
		<title>Blogs</title>
		<meta name="description" content="">
		<meta name="author" content="">
                <link rel="shortcut icon" href="images/fevicon.png">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                 <?php include './includes/cssfiles.php';?> 
                <style>
                    
.testimonial-image {
    width: 64px;
    /* margin: 0 auto; */
    margin-top: -42px;
    /* margin-right: -50px; */
}
                    .btn-menu{
                        margin-left: 10px;
                    }
                    .btn-default-transparent:active,.btn-default-transparent:focus{
                        background:none;
                            color: #777;
                    }
                    .tips {
                    display: inline-block;
                    position: relative;
                    width: auto;
                    min-width: 180px;
                    height: auto;
                    vertical-align: top;
                    margin-left: 35px;
                    margin-right: 35px;
                    margin-bottom: 50px;
                    }
                    .innerTip {
                    display: inline-block;
                    vertical-align: middle;
                    width: 100%;
                    margin-left:auto;
                    margin-right:auto;
                    text-align:center;
                    }
                    .innerTipTitle {
                    font-family:century gothic;
                    font-weight:bold;
                    
                    color: #007833;
                    font-size: 16px;
                    line-height: 20px;
                    text-align: center;
                    text-decoration: none;
                    vertical-align: top;
                    padding-top: 10px;
                    }
                    #steps li{
                        font-family:century gothic;
                        font-size:24px;
                        font-weight:bold;
                        
                    }
                    #steps li a{
                        font-family:century gothic;
                        font-size:16px;
                        font-weight:bold;
                        background:transparent;
                       
                       
                    }
                    #steps li a:active{
                        background:orangered;
                        color:white;
                         background:transparent;
                    }
                    #stepContent{
                        position:relative;
                        top:30px;
                        background:#e9ebee;
                    }
                    #stepImg{
                        cursor:pointer;
                    }
                    #stepImg_2{
                        cursor:pointer;
                        height: 200px;
                    }
                    #stepContentInner{
                        background:#e9ebee;
                    }
                                    #footer-top {
                                    background: #0d5995;
                                    min-height: 40px;
                                    border-bottom: 1px solid white;
                                    height: auto;
                                    color: white;
                                    margin-top: 1px;
                                    padding-top: 15px;
                                    box-shadow: inset 0 4px 10px rgba(0,0,0, 0.50);
                                    border-bottom: 1px solid white;
                                    }
                                    #menuBar{
                                        position:relative;
                                        float:right;
                                        top:3px;
                                        font-size:16px;
                                        color:black;
                                        cursor:pointer;
                                        
                                    }
                                    #menuBar:hover{
                                        color:#0d5995;
                                        
                                    }
                                    #request{
                                        position:relative;
                                        
                                        margin-right:10px;
                                        margin-left:10px;
                                        font-weight:bold;
                                    }
                                    .scrollToDown{
                                        text-align: center;
                                        color: #fff;
                                        font-size:21px;
                                        font-weight:bold;
                                        position: fixed;
                                        padding-top:10px;
                                        bottom:60px;
                                        right:5px;
                                        width: 50px;
                                        height: 50px;
                                        cursor: pointer;
                                        background-color: rgba(0,0,0,.4);
                                        z-index: 1005;
                                        -webkit-backface-visibility: hidden;
                                        -webkit-transform: translateZ(0);
                                        -webkit-transition: all .2s ease-in-out;
                                        -o-transition: all .2s ease-in-out;
                                        transition: all .2s ease-in-out;
                                    }


                                
                </style>
	</head>

	
	<body class="no-trans  ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
                   
			<!-- Offcanvas side start -->
                       
			<!-- offcanvas side end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">
                            <?php include 'includes/header.php';?>
			<div class="banner dark-translucent-bg" style="background-image:url('images/background-img-3.jpg'); background-position: 50% 32%;">
				<a href="#homeRow1" id="scrollDownButton"><div class="scrollToDown circle"  style="display: block;"><i class="icon-down-open-big"></i></div></a>
				
			</div>
			<!-- banner end -->

			<!-- main-container start -->
			<!-- ================ -->
                        
			<section class="pv-30 clearfix object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100" style="background:#f5f5f5;" >
				<div class="container">
                                    <center id="category_tbody"><a href="#" class="btn btn-gray-transparent">Transparent</a></center>
                                     <div class="row">
                                         <div class="col-md-11" >
                                             <div class="timeline clearfix" id="blog_details_client"></div>
                                         </div>
                                         
<!--                                     <div class="col-md-3" style="position:relative;top:80px;">
                                     <div class="image-box style-2 mb-20 shadow bordered light-gray-bg text-center" >
                                         <h3 class="heading-font text-center" style="background:#4c4c4c;color:white;padding-top:10px;padding-bottom:10px">Categories</h3> 
                                         <div class="body">
                                            <table class="table table-striped ">
                                                 <tbody id="category_tbody">
                                                     
                                                 
                                                 </tbody>
                                             </table>
                                          
                                             
                                         </div>
                                     </div>
                                      
                                      
                                 </div> -->
                                     </div>
					
                                    
				</div>
                             
                           
                         
			</section>
<?php include 'includes/footer.php'; ?>
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<?php include 'includes/jsfiles.php'; ?>
                <script type="text/javascript" src="ajax/QuickQuery.js"></script>
                <?php include 'includes/support.php'; ?>
 
        
        
        <script>
            var check = true;
            $("body").click(function(){
                  if(!check){
                    closeNav(); 
                }
                   
                  
               });
            var plan_for_yo={};
            $(document).ready(function() {
               
               setReset();
            });
            function setReset(){
                plan_for_yo={};
                $("#steps").html('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-user pr-10" ></i><b id="title_list_tab">Select Your Gender</b></a></li>');
                $("#htab1").find("div:first-child").html('<div class="col-md-6 col-xs-6"><center><img onclick="setMale1();" src="images/icon/b-nutirifed-1.png" id="stepImg"/></center><p style="font-family:century gothic;text-align:center;margin-top:10px;font-weight:bold;" >Male</p></div><div class="col-md-6 col-xs-6"><center><img onclick="setFeMale1();" src="images/icon/b-nutirifed-2.png" id="stepImg"/></center><p style="font-family:century gothic;text-align:center;margin-top:10px;font-weight:bold;">Female</p> </div>');
                $("#letsgo").hide();    
            }
            function setMale1(){
                plan_for_yo['gender']="male";
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-male pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Select Your Goal");
                $("#htab1").find("div:first-child").html('<div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_loss();" src="images/icon/b-nutirifed-5.png" id="stepImg_2"/></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_mgmt();" src="images/icon/b-nutirifed-7.png" id="stepImg_2"/></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_gain();" src="images/icon/b-nutirifed-9.png" id="stepImg_2"/></center></div>');
            }
             function setFeMale1(){
                plan_for_yo['gender']="Female";
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-female pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Select Your Goal");
                $("#htab1").find("div:first-child").html('<div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_loss();" src="images/icon/b-nutirifed-3.png" id="stepImg_2" /></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_mgmt();" src="images/icon/b-nutirifed-6.png" id="stepImg_2" /></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_gain();" src="images/icon/b-nutirifed-8.png" id="stepImg_2" /></center></div>');
            }
            function setMale1_weight_loss(){
                plan_for_yo['goal']="weight_loss";
                setMale_activity();
            }
            function setMale1_weight_mgmt(){
                plan_for_yo['goal']="weight_mgmt";
                setMale_activity();
            }
            function setMale1_weight_gain(){
                plan_for_yo['goal']="weight_gain";
                setMale_activity();
            }
            function setMale_activity(){
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-bullseye pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Enrolled for any fitness activity ");
                $("#htab1").find("div:first-child").html('<div class="col-md-12 col-xs-8"> <center> <div class="form-group"> <label for="inputEmail3" class="control-label"></label> <br><div class="btn-group btn-toggle"> <button class="btn btn-xs btn-default-transparent" style="color:white" onclick="setOn()">Yes</button> <button class="btn btn-xs btn-default-transparent"style="color:white" onclick="setOff()">No</button> </div></div><div class="form-group" style="display:none"> <label for="inputEmail3" class="control-label">Please mention the Type of fitness in the below field</label> <input type="text" class="form-control" style="width:90%" id="id_on_off" placeholder="Gym, yoga”" disabled="true"> </div><div class="form-group"> <button href="#" class=" btn btn-lg btn-gray-transparent " id="id_next" onclick="setContactForm()" style="display:none;">Next</button> </div><label class="control-label" id="error_msg_id"></label> </center></div>');
            }
            var toogle=false;
            function setOn(){
                toogle=true;
                $("#id_on_off").parent().css({'display':'block'})
                $("#id_next").css({'display':'block'})
                $("#id_on_off").prop("disabled",false);
            }
            function setOff(){
                toogle=false;
                $("#id_on_off").parent().css({'display':'none'})
                $("#id_on_off").prop("disabled",true);
                plan_for_yo['fitness_activity']="No";
                setContactForm();
            }
            plan_for_yo['fitness_activity']="No";
            function setContactForm(){
                if(toogle){
                   if($("#id_on_off").val()!="No"){
                        plan_for_yo['fitness_activity']=$("#id_on_off").val();
                   }else{
                       $("#error_msg_id").html("Please mention the Type of fitness .").css({'color':'red'});
                       return false;
                   } 
                }
//                alert(JSON.stringify(plan_for_yo))
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-check pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Contact ");
                $("#htab1").find("div:first-child").html($('<div class="col-md-12"> <form style="margin-right: 19px;" class="form-horizontal" role="form" id="step_details"> <center> <h3>About You</h3></center> <div class="form-group"></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Name</label> <div class="col-sm-9"> <input type="text" class="form-control" id="name" name="name" placeholder="Your Name *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Email</label> <div class="col-sm-9"> <input type="email" id="email" name="email" class="form-control" placeholder="Email *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Contact No</label> <div class="col-sm-9"> <input type="text" id="contact" name="contact" class="form-control" placeholder="Contact *"> </div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Location</label> <div class="col-sm-9"> <input type="text" class="form-control" id="location" name="location" placeholder="Location eg. Koramangala *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Date of Birth</label> <div class="col-sm-9"> <input type="date" class="form-control" id="dob" name="dob"> </div></div><div class="form-group"> <center> <p id="errorMsg" style="color:red;font-family:verdana;font-weight:bold"></p></center> </div></form></div>'));
                $("#letsgo").show();
            }
        </script>
        <script type="text/javascript" src="ajax/Step.js"></script>
        
<script>
            blog={}
            temp_date={}
            $(document).ready(function(){
                var cta_id="";
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogCategoryDetails.php",
                      success: function(data) {
//                          alert(data)
                        $("#category_tbody").empty();
                        var duce = jQuery.parseJSON(data);
                        $.each(duce, function (index, article) {
                        if(cta_id==""){
                            cta_id=article.blog_category_details_id;
                        }
                            $("#category_tbody").append($("<a>").addClass("btn-menu btn btn-gray-transparent").append(article.category+" ("+article.counter+")").css({'cursor':'pointer'})).click(function (){
                                        onlad(article.blog_category_details_id)
                                    });   
                        })
                        
                    }
                })
                if(urlParam('id')==null){
                    onlad("");
                }else{
                    onlad(urlParam('id'));
                }
            });
            function urlParam(name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}
            function onlad(category_id){
                /* Ajax call for Product Display*/
                    $.ajax({
                    type:"post",
                    url:"server/controller/SelBlogDetails.php",
                    data:{'category_id':category_id},
                    success: function(data) {
                        blog={}
                        temp_date={}
                        var count=0;
//                        alert(data)
                       var duce = jQuery.parseJSON(data);
                        $("#blog_details_client").empty();
                        $.each(duce, function (index, article) {
                            blog[article.blog_details_id]={};
                            blog[article.blog_details_id]["user_id"]=article.user_id;
                            blog[article.blog_details_id]["title"]=article.title;
                            blog[article.blog_details_id]["short_description"]=article.short_description;
                            blog[article.blog_details_id]["long_description"]=article.long_description;
                            blog[article.blog_details_id]["date"]=article.date;
                            blog[article.blog_details_id]["category_id"]=article.category_id;
                            blog[article.blog_details_id]["status"]=article.status;
                            blog[article.blog_details_id]["pic"]=article.pic;
                            blog[article.blog_details_id]["link"]=article.link;
                            
                           var img= "server/controller/"+article.pic;
                           var btn_class="btn-danger";
                           if(article.status=="Enable"){
                               var cl=""
                                if(count%2==1){
                                    cl=" pull-right"
                                }
                                var datearray = article.date.split("-");
                                var newdate = datearray[1] + '-' + datearray[0] + '-' + datearray[2];
                                var dateObj = new Date(newdate);
                                var month = dateObj.toLocaleString("en-in", { month: "long" });
                                var day = dateObj.getUTCDate();
                                var year = dateObj.getUTCFullYear();
                                if(temp_date[month+year]==null){
                                    temp_date[month+year]={}
                                    $("#blog_details_client").append($("<div>").addClass("timeline-date-label clearfix").append(month+" "+year));
                                    cl="";
                                }
                                $("#blog_details_client").append($("<div>").addClass("timeline-item"+cl)
                                                .append($("<article>").addClass("blogpost shadow light-gray-bg bordered")
                                                    .append($("<div>").addClass("overlay-container")
                                                        .append($("<img>").addClass("img-responsive").attr({'src':img,'width':'500','height':'100'}).css({'width': '500px','height': '150px'}))
                                                        .append($("<a>").addClass("overlay-link")))
                                                    .append($("<header>").append($('<div class="testimonial-image pull-right"><img src="server/controller/'+article.a_pic+'" alt="Jane Doe" title="Jane Doe" class="img-circle"></div>'))
                                                        .append($("<h2>").addClass("img-responsive")
                                                            .append($("<a>").append(article.title)))
                                                        )
                                                    .append($("<div>").addClass("blogpost-content").append(article.short_description))
                                                    .append($("<footer>").addClass("clearfix")
                                                        .append($("<div>").addClass("link pull-right")
                                                            .append($("<i>").addClass("icon-link"))
                                                            .append($("<a>").append("Read More").css({'cursor':'pointer'}))
                                                        ))
                                                ).click(function (){
                                                                 window.location=article.fb;
                                                            }).css({'cursor':'pointer'}));
                            count++;        
                           }
                          
                        });
                    }
                });
                }
                var screen_width = $(window).width();
                if(screen_width>768){
                  $("#blog_details_client").parent().css({'margin-left':'50px'});
                }
            $(window).resize(function() {
              if(screen_width>768){
                  $("#blog_details_client").parent().css({'margin-left':'50px'});
                }else{
                    $("#blog_details_client").parent().css({'margin-left':'0px'});
                }
              
            });
            
</script>        
</html>


