  <footer id="footer" class="clearfix ">
                            <div class="container-fluid" id="footer-top"></div>
				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer" id="footer-middle">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <h2 class="title">Our Motto</h2>
                                                                            <div class="separator-2"></div>
										
                                                                            <p id="cpara"><b>"We strive for  healthier Earth & Humans “inch by inch!” </b><br><a href="about-us.php">Learn More<i class="fa fa-long-arrow-right pl-5"></i></a></p>
										<div class="separator-2"></div>
										<ul class="list-icons" style="font-weight:bold">
                                                                                    <li><i class="fa fa-book pr-10 text-default"></i><a href="privacy-policy.php" style="color:#777">Privacy Policy</a></li>
                                                                <div class="separator-2"></div>
                                                                <li><i class="fa fa-book pr-10 text-default"></i><a href="terms-n-condition.php" style="color:#777">Terms & Condition</a></li>
                                                                <div class="separator-2"></div>
                                                                
                                                                        </ul>
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <h2 class="title">Quick Links</h2>
                                                                            <div class="separator-2"></div>
                                                                            <ul class="list-icons" style="font-weight:bold">
                                                                                        <li><i class="fa fa-book pr-10 text-default"></i><a href="healthy-food.php" style="color:#777">Healthy Food</a></li>
<!--                                                                                        <div class="separator-2"></div>
                                                                                        <li><i class="fa fa-book pr-10 text-default"></i><a href="services.php" style="color:#777">Services</a></li>-->
                                                                                        <div class="separator-2"></div>
                                                                                        <li><i class="fa fa-book pr-10 text-default"></i><a href="about-us.php" style="color:#777">About Us</a></li>
                                                                                        <div class="separator-2"></div>
                                                                                        <!--<li><i class="fa fa-book pr-10 text-default"></i><a href="supplements.php" style="color:#777">Supplements</a></li>-->
                                                                                        <li><i class="fa fa-book pr-10 text-default"></i><a href="pricing.php" style="color:#777">Pricing</a></li>
                                                                                        <div class="separator-2"></div>
                                                                                        <li><i class="fa fa-book pr-10 text-default"></i><a href="blog.php" style="color:#777">Blog</a></li>
                                                                            </ul>
                                                                            
										<div class="separator-2"></div>
										
									</div>
								</div>
								<div class="col-md-3">
									<div class="footer-content">
                                                                            <h2 class="title">Find us on Social Media</h2>
                                                                            <div class="separator-2"></div>
                                                                            <ul class="social-links circle animated-effect-1">
											<li class="facebook"><a target="_blank" href="https://www.facebook.com/bnutrified"><i class="fa fa-facebook"></i></a></li>
											<li class="twitter"><a target="_blank" href="https://twitter.com/bnutrified"><i class="fa fa-twitter"></i></a></li>
											<li class="instagram"><a target="_blank" href="https://www.instagram.com/bnutrified/"><i class="fa fa-instagram"></i></a></li>
											<li class="youtube"><a target="_blank" href="https://www.youtube.com/channel/UCn42KqmcFAf-IxMQ_40AVwQ"><i class="fa fa-youtube-play"></i></a></li>
										</ul>
                                                                            <div class="separator-2"></div>
                                                                            
										
										
									</div>
								</div>
								<div class="col-md-3">
                                                                                                                  <div class="footer-content">
                                                                            <h2 class="title">Contact Us</h2>
                                                                            <div class="separator-2"></div>

                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
												<i class="fa fa-envelope fa-2x pr-10 text-default"></i>
											</div>
											<div class="media-body">
                                                                                            <h5 class="media-heading" style="line-height:24px;font-family:century gothic;text-transform:none">connect@bnutrified.com</h5>
											</div>
											
										</div>
                                                                             <div class="separator-2"></div>
                                                                            <div class="media margin-clear" style="">
											<div class="media-left">
                                                                                            <i class="fa fa-phone fa-2x pr-10 text-default"></i>
												
											</div>
											<div class="media-body">
                                                                                            <h5 class="media-heading" style="line-height:24px;font-family:century gothic;text-transform:none">  8660984585 , 9742022399 </h5>
											</div>
											
										</div>
                                                                             <div class="separator-2"></div>
                                                                            
                                                                            
										
										
									</div>
                                                    
                                                       
									
								</div>

							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter" id="footer-top">
					<div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
                                                                    <p id="cpara2">Copyright © 2017 <b>GladdenOrb Pvt. Ltd</b>. All Rights Reserved. 
                                                                        <!--Powered by <a href="http://infoparkinnovations.in" target="_blank" style="color:white"><b>Infopark</b></a>-->
                                                                    </p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .subfooter end -->

			</footer>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/592daecfb3d02e11ecc677ab/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->