<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class DietCategoryDetailsMgr{    
        //method to insert diet_category_details in database
       public function insDietCategoryDetails(DietCategoryDetails $diet_category_details){
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO diet_category_details("
                    . "diet_category_details_id,"
                    . "diet_category)"
                    . "VALUES ('".$diet_category_details->getDiet_category_details_id()."',"
                    . "'".$diet_category_details->getDiet_category()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
       //method to select DietCategoryDetails from database
        public function selDietCategoryDetails() {
            $dbh = new DatabaseHelper();
            $sql = "select * from diet_category_details order by diet_category_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
      
        //        method to update DietCategoryDetails in database
        public function updateDietCategoryDetails(DietCategoryDetails $diet_category_details){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE diet_category_details SET " 
                    ."diet_category='".$diet_category_details->getDiet_category()."'"
                     ."WHERE diet_category_details_id=".$diet_category_details->getDiet_category_details_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        } 
          //method to delete diet_category_details in database
        public function delDietCategoryDetails($diet_category_details_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from diet_category_details where diet_category_details_id = '".$diet_category_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


