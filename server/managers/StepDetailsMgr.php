<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
        class StepDetailsMgr{    

        //method to insert step_details in database
        public function insStepDetails(StepDetails $step_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO step_details( "
                    . "gender,"
                    . "goal,"
                    . "fitness_activity,"
                    . "name,"
                    . "email,"
                    . "contact,"
                    . "location,"
                    . "dob,"
                    . "date,"
                    . "height,"
                    . "weight) "
                    . "VALUES ('".$step_details->getGender()."',"
                    . "'".$step_details->getGoal()."',"
                    . "'".$step_details->getFitness_activity()."',"
                    . "'".$step_details->getName()."',"
                    . "'".$step_details->getEmail()."',"
                    . "'".$step_details->getContact()."',"
                    . "'".$step_details->getLocation()."',"
                    . "'".$step_details->getDob()."',"
                    . "'".$step_details->getDate()."',"
                    . "'".$step_details->getHeight()."',"
                    . "'".$step_details->getWeight()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            echo $sql;
            if ($i > 0) {                
                return TRUE;
            } else {
                print_r($stmt->errorInfo());
                return FALSE;
            }
        }
        //method to select StepDetails from database
        public function selStepDetails() {
            $dbh = new DatabaseHelper();
            $sql = "select * FROM step_details as qq";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
           
             //method to delete Enquiry in database
        public function delStepDetails($step_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from step_details where step_details_id = '".$step_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }  
    }
?>
