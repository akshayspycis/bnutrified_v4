<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class ExamsPhasesMgr {    
        //method to insert examsphases in database
        public function insExamsPhases(ExamsPhases $examsphases) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO examsphases(exam_phase_name,exam_id,exam_sub_cat_id) VALUES ('".$examsphases->getExam_phase_name()."','".$examsphases->getExam_id()."','".$examsphases->getExam_sub_cat_id()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

      
        
        //method to select ExamsPhases from database
        public function selExamsPhases() {
            $dbh = new DatabaseHelper();
           
            $sql = "SELECT *,(select exam_name from exams where exam_id=ep.exam_id) as exam_name "
                    . ",(select exam_sub_cat_name from examssubcategory where exam_sub_cat_id=ep.exam_sub_cat_id) as exam_sub_cat_name FROM examsphases as ep";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //method to select ExamsPhases from database
        public function selExamsPhases2($exam_sub_cat_id) {
            $dbh = new DatabaseHelper();
           
            $sql = "select * from examsphases where exam_sub_cat_id ='".$exam_sub_cat_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update ExamsPhases in database
        public function updateExamsPhases(ExamsPhases $examsphases) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE examsphases SET " 
                    ."exam_sub_cat_name='".$examsphases->getExam_sub_cat_name()."',"
                    ."exam_id='".$examsphases->getExam_id()."'"
                     ."WHERE exam_sub_cat_id=".$examsphases->getExam_sub_cat_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete examsphases in database
        public function delExamsPhases($exam_phase_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from  examsphases where exam_phase_id = '".$exam_phase_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


