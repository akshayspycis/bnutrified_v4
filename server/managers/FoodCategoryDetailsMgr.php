
<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class FoodCategoryDetailsMgr{    

        //method to insert food_category_details in database
        public function insFoodCategoryDetails(FoodCategoryDetails $food_category_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO food_category_details( "
                    . "food_category) "
                    . "VALUES ('".$food_category_details->getFood_category()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delFoodCategoryDetails($food_category_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from food_category_details where food_category_details_id = '".$food_category_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select FoodCategoryDetails from database
        public function selFoodCategoryDetails( ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from food_category_details b";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateFoodCategoryDetails(FoodCategoryDetails $food_category_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE food_category_details SET " 
                    ."food_category='".$food_category_details->getFood_category()."'"
                    ."WHERE food_category_details_id=".$food_category_details->getFood_category_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
