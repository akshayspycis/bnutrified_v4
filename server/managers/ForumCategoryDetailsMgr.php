

<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class ForumCategoryDetailsMgr{    

        //method to insert forum_category_details in database
        public function insForumCategoryDetails(ForumCategoryDetails $forum_category_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO forum_category_details( "
                    . "forum_category, "
                    . "category_url) "
                    . "VALUES ('".$forum_category_details->getForum_category()."',"
                    . "'".$forum_category_details->getCategory_url()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delForumCategoryDetails($forum_category_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from forum_category_details where forum_category_details_id = '".$forum_category_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select ForumCategoryDetails from database
        public function selForumCategoryDetails( ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from forum_category_details b";
            //$sql = "select *,(select count(*) from blog_details bd where bd.category_id=b.forum_category_details_id) as counter from forum_category_details b";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        public function selForumCategoryDetailsInClient( ) {
            $dbh = new DatabaseHelper();
            $sql = "select *,(select count(*) from forum_topic_details ftd where ftd.forum_category_details_id=b.forum_category_details_id) as topic,
(select count(*) from forum_post_details fpd where fpd.forum_category_details_id=b.forum_category_details_id) as post,
(select ftd.topic_url from forum_topic_details ftd where ftd.forum_category_details_id=b.forum_category_details_id
order by STR_TO_DATE(date, '%d-%m-%Y') limit 1) as latest
from forum_category_details b";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateForumCategoryDetails(ForumCategoryDetails $forum_category_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE forum_category_details SET " 
                    ."forum_category='".$forum_category_details->getForum_category()."',"
                    ."category_url='".$forum_category_details->getCategory_url()."'"
                    ."WHERE forum_category_details_id=".$forum_category_details->getForum_category_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          
    }
?>
