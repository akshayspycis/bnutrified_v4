<?php
    include_once '../dbhelper/DatabaseHelper.php';
    class AddressDetailsMgr{    
        //method to insert address_details in database
        public function insAddressDetails(AddressDetails $address_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO address_details( "
                    . "address_type,"
                    . "address,"
                    . "street,"
                    . "city,"
                    . "pincode,"
                    . "state,"
                    . "course_registration_id) "
                    . "VALUES ('".$address_details->getAddress_type()."',"
                    . "'".$address_details->getAddress()."',"
                    . "'".$address_details->getStreet()."',"
                    . "'".$address_details->getCity()."',"
                    . "'".$address_details->getPincode()."',"
                    . "'".$address_details->getState()."',"
                    . "(SELECT MAX(course_registration_id) FROM course_registration))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delAddressDetails($address_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from address_details where address_details_id = '".$address_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select AddressDetails from database
        public function selAddressDetails($course_registration_id) {
            $dbh = new DatabaseHelper();
            $sql= "select * from address_details where course_registration_id=".$course_registration_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
  public function updateAddressDetails(AddressDetails $address_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE address_details SET " 
                    ."course_id='".$address_details->getCourse_id()."',"
                    ."date='".$address_details->getDate()."',"
                    ."day_type='".$address_details->getDay_type()."',"
                    ."day='".$address_details->getDay()."',"
                    ."time='".$address_details->getTime()."',"
                    ."fees='".$address_details->getFees()."',"
                    ."status='".$address_details->getStatus()."',"
                    ."discount='".$address_details->getDiscout()."'"
                    ."WHERE address_details_id=".$address_details->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updAddressDetailstatus(AddressDetails $address_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE address_details SET " 
                    ."status='".$address_details->getStatus()."'"
                   ."WHERE address_details_id=".$address_details->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
