<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class FoodDetailsMgr{    

        //method to insert food_details in database
        public function insFoodDetails(FoodDetails $food_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO food_details( "
                    . "title, "
                    . "pic, "
                    . "discription, "
                    . "price, "
                    . "status, "
                    . "food_category_details_id) "
                    . "VALUES ('".$food_details->getTitle()."',"
                    . "'".$food_details->getPic()."',"
                    . "'".$food_details->getDiscription()."',"
                    . "'".$food_details->getPrice()."',"
                    . "'".$food_details->getStatus()."',"
                    . "'".$food_details->getFood_category_details_id()."')";;
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delFoodDetails($food_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from food_details where food_details_id = '".$food_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select FoodDetails from database
        public function selFoodDetails($food_category_details_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from food_details where food_category_details_id='".$food_category_details_id."' order by food_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        public function selFoodDetailsClient() {
            $dbh = new DatabaseHelper();
            $sql = "select * from food_details where status='Enable' order by food_details_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        
        
        public function updFoodDetailstatus(FoodDetails $food_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE food_details SET " 
                    ."status='".$food_details->getStatus()."'"
                   ." WHERE food_details_id=".$food_details->getFood_details_id()."";
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
