<?php
    include_once '../dbhelper/DatabaseHelper.php';
    class CourseOptedDetailsMgr{    
        //method to insert course_opted_details in database
        public function insCourseOptedDetails(CourseOptedDetails $course_opted_details) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO course_opted_details( "
                    . "course_po_mt_and_clerk,"
                    . "advance_comm_english,"
                    . "ssc_and_railways,"
                    . "english_for_competitive_exams,"
                    . "personal_interviews,"
                    . "mba_entrance_exam,"
                    . "course_registration_id) "
                    . "VALUES ('".$course_opted_details->getCourse_po_mt_and_clerk()."',"
                    . "'".$course_opted_details->getAdvance_comm_english()."',"
                    . "'".$course_opted_details->getSsc_and_railways()."',"
                    . "'".$course_opted_details->getEnglish_for_competitive_exams()."',"
                    . "'".$course_opted_details->getPersonal_interviews()."',"
                    . "'".$course_opted_details->getMba_entrance_exam()."',"
                    . "(SELECT MAX(course_registration_id) FROM course_registration))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delCourseOptedDetails($course_opted_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from course_opted_details where course_opted_details_id = '".$course_opted_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select CourseOptedDetails from database
        public function selCourseOptedDetails($course_registration_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from course_opted_details where course_registration_id=".$course_registration_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateCourseOptedDetails(CourseOptedDetails $course_opted_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE course_opted_details SET " 
                    ."course_id='".$course_opted_details->getCourse_id()."',"
                    ."date='".$course_opted_details->getDate()."',"
                    ."day_type='".$course_opted_details->getDay_type()."',"
                    ."day='".$course_opted_details->getDay()."',"
                    ."time='".$course_opted_details->getTime()."',"
                    ."fees='".$course_opted_details->getFees()."',"
                    ."status='".$course_opted_details->getStatus()."',"
                    ."discount='".$course_opted_details->getDiscout()."'"
                    ."WHERE course_opted_details_id=".$course_opted_details->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updCourseOptedDetailstatus(CourseOptedDetails $course_opted_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE course_opted_details SET " 
                    ."status='".$course_opted_details->getStatus()."'"
                   ."WHERE course_opted_details_id=".$course_opted_details->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
