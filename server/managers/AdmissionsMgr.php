<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class AdmissionsMgr {    
        //method to insert admissions in database
        public function insAdmissions(Admissions $admissions) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO admissions("
                    . "class_name, "
                    . "first_name, "
                    . "middle_name, "
                    . "last_name, "
                    . "dob, "
                    . "gender, "
                    . "category, "
                    . "cast, "
                    . "nationality, "
                    . "student_samgraid, "
                    . "aadhar_no, "
                    . "family_samgraid, "
                    . "contact, "
                    . "email, "
                    . "school_name, "
                    . "address, "
                    . "locality, "
                    . "village, "
                    . "grampanchayat, "
                    . "nagarpanchayat, "
                    . "district, "
                    . "pincode, "
                    . "state, "
                    . "image, "
                    . "mothers_name, "
                    . "mothers_qualifications, "
                    . "mothers_occupation, "
                    . "mothers_contact, "
                    . "fathers_name, "
                    . "fathers_qualifications, "
                    . "fathers_occupation, "
                    . "fathers_contact, "
                    . "fathers_emailid, "
                    . "local_guardian, "
                    . "local_guardian_contact, "
                    . "local_guardian_occupation, "
                    . "last_school_attended, "
                    . "last_school_scholar, "
                    . "last_class, "
                    . "passed_year, "
                    . "passed_board, "
                    . "declaration) VALUES ('".$admissions->getClass_name()."',"
                                              . "'".$admissions->getFirst_name()."',"
                                              . "'".$admissions->getMiddle_name()."',"
                                              . "'".$admissions->getLast_name()."',"
                                              . "'".$admissions->getDob()."',"
                                              . "'".$admissions->getGender()."',"
                                              . "'".$admissions->getCategory()."',"
                                              . "'".$admissions->getCast()."',"
                                              . "'".$admissions->getNationality()."',"
                                              . "'".$admissions->getStudent_samgraid()."',"
                                              . "'".$admissions->getAadhar_no()."',"
                                              . "'".$admissions->getFamily_samgraid()."',"
                                              . "'".$admissions->getContact()."',"
                                              . "'".$admissions->getEmail()."',"
                                              . "'".$admissions->getSchool_name()."',"
                                              . "'".$admissions->getAddress()."',"
                                              . "'".$admissions->getLocality()."',"
                                              . "'".$admissions->getVillage()."',"
                                              . "'".$admissions->getGrampanchayat()."',"
                                              . "'".$admissions->getNagarpanchayat()."',"
                                              . "'".$admissions->getDistrict()."',"
                                              . "'".$admissions->getPincode()."',"
                                              . "'".$admissions->getState()."',"
                                              . "'".$admissions->getImage()."',"
                                              . "'".$admissions->getMothers_name()."',"
                                              . "'".$admissions->getMothers_qualifications()."',"
                                              . "'".$admissions->getMothers_occupation()."',"
                                              . "'".$admissions->getMothers_contact()."',"
                                              . "'".$admissions->getFathers_name()."',"
                                              . "'".$admissions->getFathers_qualifications()."',"
                                              . "'".$admissions->getFathers_occupation()."',"
                                              . "'".$admissions->getFathers_contact()."',"
                                              . "'".$admissions->getFathers_emailid()."',"
                                              . "'".$admissions->getLocal_guardian()."',"
                                              . "'".$admissions->getLocal_guardian_contact()."',"
                                              . "'".$admissions->getLocal_guardian_occupation()."',"
                                              . "'".$admissions->getLast_school_attended()."',"
                                              . "'".$admissions->getLast_school_scholar()."',"
                                              . "'".$admissions->getLast_class()."',"
                                              . "'".$admissions->getPassed_year()."',"
                                              . "'".$admissions->getPassed_board()."',"
                                              . "'".$admissions->getDeclaration()."',"
                                              . "'".$admissions->getDatetime()."')";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete admissions in database
        public function delAdmissions($admissions_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from admissions where admissions_id = '".$admissions_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Admissions from database
        public function selAdmissions($school_name,$class_name) {
            $dbh = new DatabaseHelper();
            $sql = "select * from admissions where school_name='".$school_name."' and class_name='".$class_name."' ORDER BY admissions_id DESC " ;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update Admissions in database
        public function updateAdmissions(Admissions $admissions) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE admissions SET " 
                    ."heading='".$admissions->getHeading()."',"
                    ."content='".$admissions->getContent()."'"
                    ."WHERE admissions_id=".$admissions->getId()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
         
    }
?>

