
<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class DemoClassMgr{    
        public function insDemoClass(DemoClass $demo_class) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO demo_class( "
                    . "demo_class, "
                    . "status) "
                    . "VALUES ('".$demo_class->getDemo_class()."',"
                    . "'".$demo_class->getStatus()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function delDemoClass($demo_class_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from demo_class where demo_class_id = '".$demo_class_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        public function selDemoClass( ) {
            $dbh = new DatabaseHelper();
            $sql = "select * from demo_class ";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
  public function updateDemoClass(DemoClass $demo_class) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE demo_class SET " 
                    ."demo_class='".$demo_class->getDemo_class()."',"
                    ."status='".$demo_class->getStatus()."'"
                    ."WHERE demo_class_id=".$demo_class->getDemo_class_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
    }
?>
