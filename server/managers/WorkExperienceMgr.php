<?php
    include_once '../dbhelper/DatabaseHelper.php';
    class WorkExperienceMgr{    
        //method to insert work_experience in database
        public function insWorkExperience(WorkExperience $work_experience) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO work_experience( "
                    . "organization,"
                    . "time,"
                    . "designation,"
                    . "course_registration_id) "
                    . "VALUES ('".$work_experience->getOrganization()."',"
                    . "'".$work_experience->getTime()."',"
                    . "'".$work_experience->getDesignation()."',"
                    . "(SELECT MAX(course_registration_id) FROM course_registration))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delWorkExperience($work_experience_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from work_experience where work_experience_id = '".$work_experience_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select WorkExperience from database
        public function selWorkExperience($course_registration_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from work_experience where course_registration_id=".$course_registration_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateWorkExperience(WorkExperience $work_experience) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE work_experience SET " 
                    ."course_id='".$work_experience->getCourse_id()."',"
                    ."date='".$work_experience->getDate()."',"
                    ."day_type='".$work_experience->getDay_type()."',"
                    ."day='".$work_experience->getDay()."',"
                    ."time='".$work_experience->getTime()."',"
                    ."fees='".$work_experience->getFees()."',"
                    ."status='".$work_experience->getStatus()."',"
                    ."discount='".$work_experience->getDiscout()."'"
                    ."WHERE work_experience_id=".$work_experience->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updWorkExperiencetatus(WorkExperience $work_experience) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE work_experience SET " 
                    ."status='".$work_experience->getStatus()."'"
                   ."WHERE work_experience_id=".$work_experience->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
