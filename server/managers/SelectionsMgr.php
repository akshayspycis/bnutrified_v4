<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
    class SelectionsMgr{    

        //method to insert selections in database
        public function insSelections(Selections $selections) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO selections(name, percent, class, school_id, image, date) VALUES ('".$selections->getName()."','".$selections->getPercent()."','".$selections->getClass()."','".$selections->getSchool_id()."','".$selections->getImage()."','".$selections->getDate()."')";
            $stmt = $dbh->createConnection()->prepare($sql);   
            echo $sql;
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delSelections($id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from selections where id = '".$id."'";
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select Selections from database
        public function selSelections() {
             $dbh = new DatabaseHelper();
             $sql = "SELECT *,(SELECT name from school_category where school_id = s.school_id) as school_name from  selections as s";
             $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateSelections(Selections $selections) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE selections SET " 
                    ."id='".$selections->getId()."',"
                    ."name='".$selections->getName()."',"
                    ."percent='".$selections->getPercent()."',"
                    ."class='".$selections->getClass()."',"
                    ."school_id='".$selections->getSchool_id()."'"
                    ."WHERE id=".$selections->getId()."";
            echo $sql;
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          public function updSelectionsImg(Selections $selections) {
            $dbh = new DatabaseHelper();
            $sql = "SELECT selections.image FROM selections WHERE id=".$selections->getId()."";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $file;
            while($row = $stmt->fetch()) {
                $file=$row['image'];
            }
            if(file_exists($file)&&unlink($file)){
                $sql ="UPDATE selections SET " 
                ."image='".$selections->getImage()."'"
                ."WHERE id=".$selections->getId()."";
                $stmt = $dbh->createConnection()->prepare($sql);
                $i = $stmt->execute();
                $dbh->closeConnection();
                    if ($i > 0) {                
                        return TRUE;
                    } else {
                        return FALSE;
                    }
            }else{
                return FALSE;
            }
        } 
    }
?>


