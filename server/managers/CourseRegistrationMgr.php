

<?php
    include_once '../dbhelper/DatabaseHelper.php';
    
    class CourseRegistrationMgr{    

        //method to insert course_registration in database
        public function insCourseRegistration(CourseRegistration $course_registration) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO course_registration( "
                    . "dob, "
                    . "user_registration_for_demo_id, "
                    . "additional_qualification, "
                    . "computer_literacy, "
                    . "address_of_institiute_or_company, "
                    . "relative_name,"
                    . "relative_occupation,"
                    . "applicant_occupation,"
                    . "school,"
                    . "college,"
                    . "relative_contact_no,"
                    . "referal_name,"
                    . "referal_contact_no) "
                    . "VALUES ('".$course_registration->getDob()."',"
                    . "'".$course_registration->getUser_registration_for_demo_id()."',"
                    . "'".$course_registration->getAdditional_qualification()."',"
                    . "'".$course_registration->getComputer_literacy()."',"
                    . "'".$course_registration->getAddress_of_institiute_or_company()."',"
                    . "'".$course_registration->getRelative_name()."',"
                    . "'".$course_registration->getRelative_occupation()."',"
                    . "'".$course_registration->getApplicant_occupation()."',"
                    . "'".$course_registration->getSchool()."',"
                    . "'".$course_registration->getCollege()."',"
                    . "'".$course_registration->getRelative_contact_no()."',"
                    . "'".$course_registration->getReferal_name()."',"
                    . "'".$course_registration->getReferal_contact_no()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delCourseRegistration($course_registration_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from course_registration where course_registration_id = '".$course_registration_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select CourseRegistration from database
        public function selCourseRegistration() {
            $dbh = new DatabaseHelper();
            $sql = "select *,(select pic_path from bear_witness bw where attestation='Photograph' and bw.course_registration_id=cr.course_registration_id limit 1) as pic_path"
                    . " from course_registration cr "
                    . " LEFT JOIN user_registration_for_demo urfd "
                    . "on urfd.user_registration_for_demo_id= cr.user_registration_for_demo_id order by course_registration_id desc";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
  public function updateCourseRegistration(CourseRegistration $course_registration) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE course_registration SET " 
                    ."course_id='".$course_registration->getCourse_id()."',"
                    ."date='".$course_registration->getDate()."',"
                    ."day_type='".$course_registration->getDay_type()."',"
                    ."day='".$course_registration->getDay()."',"
                    ."time='".$course_registration->getTime()."',"
                    ."fees='".$course_registration->getFees()."',"
                    ."status='".$course_registration->getStatus()."',"
                    ."discount='".$course_registration->getDiscout()."'"
                    ."WHERE course_registration_id=".$course_registration->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updCourseRegistrationtatus(CourseRegistration $course_registration) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE course_registration SET " 
                    ."status='".$course_registration->getStatus()."'"
                   ."WHERE course_registration_id=".$course_registration->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
