<?php
    include_once '../dbhelper/DatabaseHelper.php';
    class LanguageProficiencyMgr{    
        //method to insert language_proficiency in database
        public function insLanguageProficiency(LanguageProficiency $language_proficiency) {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO language_proficiency( "
                    . "language_p,"
                    . "read_p,"
                    . "write_p,"
                    . "speak_p,"
                    . "course_registration_id) "
                    . "VALUES ('".$language_proficiency->getLanguage()."',"
                    . "'".$language_proficiency->getRead()."',"
                    . "'".$language_proficiency->getWrite()."',"
                    . "'".$language_proficiency->getSpeak()."',"
                    . "(SELECT MAX(course_registration_id) FROM course_registration))";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delLanguageProficiency($language_proficiency_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from language_proficiency where language_proficiency_id = '".$language_proficiency_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select LanguageProficiency from database
        public function selLanguageProficiency($course_registration_id) {
            $dbh = new DatabaseHelper();
            $sql = "select * from language_proficiency where course_registration_id=".$course_registration_id;
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateLanguageProficiency(LanguageProficiency $language_proficiency) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE language_proficiency SET " 
                    ."course_id='".$language_proficiency->getCourse_id()."',"
                    ."date='".$language_proficiency->getDate()."',"
                    ."day_type='".$language_proficiency->getDay_type()."',"
                    ."day='".$language_proficiency->getDay()."',"
                    ."time='".$language_proficiency->getTime()."',"
                    ."fees='".$language_proficiency->getFees()."',"
                    ."status='".$language_proficiency->getStatus()."',"
                    ."discount='".$language_proficiency->getDiscout()."'"
                    ."WHERE language_proficiency_id=".$language_proficiency->getCourse_batch_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updLanguageProficiencytatus(LanguageProficiency $language_proficiency) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE language_proficiency SET " 
                    ."status='".$language_proficiency->getStatus()."'"
                   ."WHERE language_proficiency_id=".$language_proficiency->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
