<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class DietPlanDetailsMgr{    
       public function insDietPlanDetails(DietPlanDetails $diet_plan_details){
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO diet_plan_details("
                    . "diet_category_details_id,"
                    . "plan, "
                    . "price, "
                    . "link)"
                    . "VALUES ('".$diet_plan_details->getDiet_category_details_id()."',"
                    . "'".$diet_plan_details->getPlan()."',"
                    . "'".$diet_plan_details->getPrice()."',"
                    . "'".$diet_plan_details->getLink()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
       //method to select DietPlanDetails from database
        public function selDietPlanDetails($diet_category_details_id) {
            $dbh = new DatabaseHelper();
            if($diet_category_details_id==""){
                $sql = "select * from diet_plan_details order by diet_plan_details_id desc";
            }else{
                $sql = "select * from diet_plan_details where diet_category_details_id='".$diet_category_details_id."' order by diet_plan_details_id desc";
            }
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
      
        //        method to update DietPlanDetails in database
        public function updateDietPlanDetails(DietPlanDetails $diet_plan_details){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE diet_plan_details SET " 
                    ."plan='".$diet_plan_details->getPlan()."',"
                    ."price='".$diet_plan_details->getPrice()."',"
                    ."link='".$diet_plan_details->getLink()."'"
                     ."WHERE diet_plan_details_id=".$diet_plan_details->getDiet_plan_details_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete diet_plan_details in database
        public function delDietPlanDetails($diet_plan_details_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from diet_plan_details where diet_plan_details_id = '".$diet_plan_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


