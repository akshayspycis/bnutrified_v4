<?php
    require_once '../dbhelper/DatabaseHelper.php';
    
    class DiscriptiveTestMgr{    
        //method to insert discriptivetest in database
       public function insDiscriptiveTest(DiscriptiveTest $discriptivetest){
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO discriptivetest("
                    . "exam_id,"
                    . "exam_sub_cat_id, "
                    . "exam_phase_id, "
                    . "papername, "
                    . "papertype, "
                    . "duration, "
                    . "maxmarks,"
                    . "year)"
                    . "VALUES ('".$discriptivetest->getExam_id()."',"
                    . "'".$discriptivetest->getExam_sub_cat_id()."',"
                    . "'".$discriptivetest->getExam_phase_id()."',"
                    . "'".$discriptivetest->getPapername()."',"
                    . "'".$discriptivetest->getPapertype()."',"
                    . "'".$discriptivetest->getDuration()."',"
                    . "'".$discriptivetest->getMaxmarks()."',"
                    . "'".$discriptivetest->getYear()."')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }
       //method to select DiscriptiveTest from database
        public function selDiscriptiveTest($discriptivetest_id) {
            
            $dbh = new DatabaseHelper();
            $sql="";
           if($discriptivetest_id==""){
               $sql = "SELECT *,(select exam_name from exams where exam_id=dt.exam_id) as exam_name "
                    . ",(select exam_sub_cat_name from examssubcategory where exam_sub_cat_id=dt.exam_sub_cat_id) as exam_sub_cat_name"
                    . ",(select exam_phase_name from examsphases where exam_phase_id=dt.exam_phase_id) as exam_phase_name"
                    . " FROM discriptivetest as dt";
           }
           else{
                $sql = "select * from discriptivetest where discriptivetest_id ='".$discriptivetest_id."'";
           }
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            
            $dbh->closeConnection();
            return $stmt;
        }
      
        //        method to update DiscriptiveTest in database
        public function updateDiscriptiveTest(DiscriptiveTest $discriptivetest){
            $dbh = new DatabaseHelper();
            $sql ="UPDATE discriptivetest SET " 
                    ."exam_sub_cat_name='".$discriptivetest->getExam_sub_cat_name()."',"
                    ."exam_id='".$discriptivetest->getExam_id()."'"
                     ."WHERE exam_sub_cat_id=".$discriptivetest->getExam_sub_cat_id()."";
                  $stmt = $dbh->createConnection()->prepare($sql);
                 $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          //method to delete discriptivetest in database
        public function delDiscriptiveTest($discriptivetest_id) {
            $dbh = new DatabaseHelper();
            $sql = "delete from  discriptivetest where discriptivetest_id = '".$discriptivetest_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        } 
    }
?>


