
<?php
    include_once '../dbhelper/DatabaseHelper.php';
        class UserPaymentDetailsMgr{    
        //method to insert user_payment_details in database
        public function insUserPaymentDetails() {
            $dbh = new DatabaseHelper();
            $sql = "INSERT INTO user_payment_details( "
                    . "work_experience_id, "
                    . "payment, "
                    . "paid, "
                    . "balance) "
                    . "VALUES ((SELECT MAX(course_registration_id) FROM course_registration),"
                    . "'0',"
                    . "'0',"
                    . "'0')";
            $stmt = $dbh->createConnection()->prepare($sql); 
            $i = $stmt->execute();            
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
                return FALSE;
            }
        }

        //method to delete news in database
        public function delUserPaymentDetails($user_payment_details_id) {
            $dbh = new DatabaseHelper();
             $sql = "delete from user_payment_details where user_payment_details_id = '".$user_payment_details_id."'";
            $stmt = $dbh->createConnection()->prepare($sql);            
            $i = $stmt->execute();            
            $dbh->closeConnection();
            
            if ($i > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }
        
        //method to select UserPaymentDetails from database
        public function selUserPaymentDetails() {
            $dbh = new DatabaseHelper();
            $sql = "select *,(select registered_temp_code from user_demo_date where user_registration_for_demo_id=urfd.user_registration_for_demo_id) as registered_temp_code,"
                    . "(select date FROM demo_date WHERE demo_date_id=(select demo_date_id from user_demo_date where user_registration_for_demo_id=urfd.user_registration_for_demo_id)) as date from user_payment_details upd "
                    . "inner join user_registration_for_demo urfd "
                    . "on urfd.user_registration_for_demo_id=(select user_registration_for_demo_id from course_registration cr where cr.course_registration_id=upd.course_registration_id)";
            
            $stmt = $dbh->createConnection()->prepare($sql);            
            $stmt->execute();
            $dbh->closeConnection();
            return $stmt;
        }
        //        method to update enquiry in database
  public function updateUserPaymentDetails(UserPaymentDetails $user_payment_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_payment_details SET " 
                    ."payment='".$user_payment_details->getPayment()."',"
                    ."paid='".$user_payment_details->getPaid()."',"
                    ."balance='".$user_payment_details->getBalance()."'"
                    ." WHERE user_payment_details_id=".$user_payment_details->getUser_payment_details_id()."";
             $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            $dbh->closeConnection();
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        }
          public function updUserPaymentDetailstatus(UserPaymentDetails $user_payment_details) {
            $dbh = new DatabaseHelper();
            $sql ="UPDATE user_payment_details SET " 
                    ."status='".$user_payment_details->getStatus()."'"
                   ."WHERE user_payment_details_id=".$user_payment_details->getCourse_batch_details_id()."";
                    
          
            
            $stmt = $dbh->createConnection()->prepare($sql);
            $i = $stmt->execute();
            
            $dbh->closeConnection();
             
            if ($i > 0) {                
                return TRUE;
            } else {
              
                return FALSE;
            }
        } 
          
    }
?>
