<?php
    include_once '../models/PrelimsExam.php'; 
    include_once '../managers/PrelimsExamMgr.php'; 
    $prelimsexam = new PrelimsExam();
    $prelimsexam->setExam_id($_POST["selExamName"]);
    $prelimsexam->setExam_sub_cat_id($_POST["selExamSubCat"]);
    $prelimsexam->setExam_phase_id($_POST["selExamPhase"]);
    $prelimsexam->setExam_section_id($_POST["selExamSection"]);
    $prelimsexam->setNo_of_questions($_POST["no_of_questions"]);
    $prelimsexam->setMax_marks($_POST["max_marks"]);
    $prelimsexam->setDuration($_POST["duration"]);
    $prelimsexam->setGen($_POST["gen"]);
    $prelimsexam->setObc($_POST["obc"]);
    $prelimsexam->setSc($_POST["sc"]);
    $prelimsexam->setSt($_POST["st"]);
    $prelimsexam->setPh($_POST["ph"]);
    $prelimsexam->setVh($_POST["vh"]);
    $prelimsexam->setYear($_POST["year"]);
    
    $prelimsexamMgr = new PrelimsExamMgr();
    if($prelimsexamMgr->insPrelimsExam($prelimsexam)) {
        echo 'Prelims Exams inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>


