<?php
    include_once '../models/FoodDetails.php'; 
    include_once '../managers/FoodDetailsMgr.php'; 
    $food_details = new FoodDetails();
    $food_details->setTitle($_POST["title"]);  
    $food_details->setDiscription($_POST["discription"]);   
    $food_details->setPrice($_POST["price"]);   
    $food_details->setStatus($_POST["status"]);   
    $food_details->setFood_category_details_id($_POST["food_category_details_id"]);   
    $upload_dir = "upload/";
    $img = $_POST["nimage"];
    $img = str_replace('data:image/png;base64,', '', $img);
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir.time().".png";
    $success = file_put_contents($file, $data);
    $food_details->setPic($file);   
    $food_detailsMgr = new FoodDetailsMgr();
    if ($food_detailsMgr->insFoodDetails($food_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>