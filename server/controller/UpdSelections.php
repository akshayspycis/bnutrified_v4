 <?php
    include_once '../models/Selections.php';
    include_once '../managers/SelectionsMgr.php';
    $selections = new Selections();    
    $selections->setId($_POST["id"]);
    $selections->setName($_POST["name"]);
    $selections->setPercent($_POST["percent"]);  
    $selections->setClass($_POST["class"]); 
    $selections->setSchool_id($_POST["school_id"]); 
    $selectionsMgr = new SelectionsMgr();    
    if ($selectionsMgr->updateSelections($selections)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>