<?php
    include_once '../managers/FoodCategoryDetailsMgr.php';
    
    $food_category_details_id = $_POST["food_category_details_id"];
    $food_category_details_mgr = new FoodCategoryDetailsMgr();
    if ($food_category_details_mgr->delFoodCategoryDetails($food_category_details_id)) {
        echo 'Food Category Deleted Successfully.';
    } else {
        echo 'Error';
    }    
?>