 <?php
    include_once '../models/Downloadsection.php';
    include_once '../managers/DownloadsectionMgr.php';
    $obj = new DownloadsectionMgr();
        
    $downloadsection = $obj->selDownloadsection();
    $str = array();    
    while($row = $downloadsection->fetch()){
    $arr = array(
    'id' => $row['id'], 
    'name' => $row['name'],             
    'path' => $row['path'],             
    'date' => $row['date'],             
    );
    array_push($str, $arr); 
    }
    echo json_encode($str);
    ?>