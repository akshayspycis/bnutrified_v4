    <?php
    include_once '../models/Feedback.php';
    include_once '../managers/FeedbackMgr.php';
    $dbh = new DatabaseHelper();
    $sql = "select * from feedback";
    $stmt = $dbh->createConnection()->prepare($sql);            
    $stmt->execute();
    $dbh->closeConnection();
    $feedback = $stmt ;
    $str = array();    
    while($row = $feedback->fetch()){
    $arr = array(
    'feedback_id' => $row['feedback_id'], 
    'name' => $row['name'],             
    'message' => $row['message'],             
    'image' => $row['image'],             
    'city' => $row['city'],             
    'date' => $row['date'],             
    'time' => $row['time'],             
    );
    array_push($str, $arr); 
    }
        
    echo json_encode($str);
    ?>