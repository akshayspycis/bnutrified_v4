<?php
     include_once '../models/AddressDetails.php';
    include_once '../managers/AddressDetailsMgr.php';
    $obj = new AddressDetailsMgr();
    $course_batch_details = $obj->selAddressDetails($_POST['course_registration_id']);
    $str = array();    
    while($row = $course_batch_details->fetch()){
        $arr = array(
            'address_type' => $row['address_type'], 
            'address' => $row['address'], 
            'street' => $row['street'],             
            'city' => $row['city'],             
            'pincode' => $row['pincode'],             
            'state' => $row['state']            
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>