<?php
     include_once '../models/FoodCategoryDetails.php';
    include_once '../managers/FoodCategoryDetailsMgr.php';
    $obj = new FoodCategoryDetailsMgr();
    $food_category_details = $obj->selFoodCategoryDetails();
    $str = array();    
    while($row = $food_category_details->fetch()){
            $arr = array(
            'food_category_details_id' => $row['food_category_details_id'], 
            'food_category' => $row['food_category']
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>