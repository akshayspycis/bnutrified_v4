 <?php
    include_once '../models/FoodCategoryDetails.php';
    include_once '../managers/FoodCategoryDetailsMgr.php';
    $food_category_details = new FoodCategoryDetails();    
    $food_category_details->setFood_category_details_id($_POST["food_category_details_id"]);
    $food_category_details->setFood_category($_POST["editcate"]);
    $food_category_detailsMgr = new FoodCategoryDetailsMgr();    
    if ($food_category_detailsMgr->updateFoodCategoryDetails($food_category_details)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>