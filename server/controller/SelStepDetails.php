<?php
     include_once '../models/StepDetails.php';
     include_once '../managers/StepDetailsMgr.php';
    $obj = new StepDetailsMgr();
    $quick_query = $obj->selStepDetails();
    $str = array();    
    while($row = $quick_query->fetch()){
            $arr = array(
            'step_details_id' => $row['step_details_id'], 
            'gender' => $row['gender'], 
            'goal' => $row['goal'],             
            'fitness_activity' => $row['fitness_activity'],             
            'name' => $row['name'],             
            'email' => $row['email'],             
            'contact' => $row['contact'],             
            'location' => $row['location'],             
            'dob' => $row['dob'],             
            'height' => $row['height'],             
            'weight' => $row['weight'],             
            'date' => $row['date']    
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>