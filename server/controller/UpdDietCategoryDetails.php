 <?php
    include_once '../models/DietCategoryDetails.php';
    include_once '../managers/DietCategoryDetailsMgr.php';
    $diet_category_details = new DietCategoryDetails();    
    $diet_category_details->setDiet_category_details_id($_POST["diet_category_details_id"]);
    $diet_category_details->setDiet_category($_POST["editcate"]);
    $diet_category_detailsMgr = new DietCategoryDetailsMgr();    
    if ($diet_category_detailsMgr->updateDietCategoryDetails($diet_category_details)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
?>