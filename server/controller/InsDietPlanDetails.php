<?php
    include_once '../models/DietPlanDetails.php'; 
    include_once '../managers/DietPlanDetailsMgr.php'; 
    $diet_plan_details = new DietPlanDetails();
    $diet_plan_details->setDiet_category_details_id($_POST["diet_category_details_id"]);
    $diet_plan_details->setPlan($_POST["plan"]);
    $diet_plan_details->setPrice($_POST["price"]);
    $diet_plan_details->setLink($_POST["link"]);
    $diet_plan_detailsMgr = new DietPlanDetailsMgr();
    if ($diet_plan_detailsMgr->insDietPlanDetails($diet_plan_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>