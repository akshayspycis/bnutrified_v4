<?php
    include_once '../models/UserRegistrationForDemo.php'; 
    include_once '../models/UserDemoDate.php'; 
    include_once '../managers/UserRegistrationForDemoMgr.php'; 
    include_once '../managers/UserDemoDateMgr.php'; 
    $user_registration_for_demo = new UserRegistrationForDemo();
    $user_registration_for_demo_josn= json_decode($_POST['user_registration_for_demo']);
    $user_registration_for_demo->setUser_name($user_registration_for_demo_josn->user_name);
    $user_registration_for_demo->setGender($user_registration_for_demo_josn->gender);  
    $user_registration_for_demo->setEmail($user_registration_for_demo_josn->email);   
    $user_registration_for_demo->setContact_no($user_registration_for_demo_josn->contact_no);   
    $user_registration_for_demoMgr = new UserRegistrationForDemoMgr();
    if ($user_registration_for_demoMgr->insUserRegistrationForDemo($user_registration_for_demo)) {
        $user_demo_date = new UserDemoDate();
        $user_demo_date->setDemo_class_id($user_registration_for_demo_josn->demo_class_id);
        $user_demo_date->setDemo_date_id($user_registration_for_demo_josn->demo_date_id);
        $user_demo_date_Mgr = new UserDemoDateMgr();
        if ($user_demo_date_Mgr->insUserDemoDate($user_demo_date)) {
            echo 'true.';
        }
    } else {
        echo 'Error';
    }
?>