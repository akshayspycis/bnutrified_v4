<?php
    include_once '../models/Feedback.php'; 
    include_once '../managers/FeedbackMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $feedback = new Feedback();
    $feedback->setName($_POST["name"]);
    $feedback->setMessage($_POST["message"]);  
    $upload_dir = "upload/";
    $img = $_POST["image"];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir . time() . ".png";
    $success = file_put_contents($file, $data);
    $feedback->setImage($file);   
    $feedback->setCity($_POST["city"]);   
    $feedback->setDate($date->format('D, d M Y'));   
    $feedback->setTime($date->format('h:i:s a'));   
    $feedbackMgr = new FeedbackMgr();
    if ($feedbackMgr->insFeedback($feedback)) {
        echo 'Feedback inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>