<?php
     include_once '../models/Admissions.php';
    include_once '../managers/AdmissionsMgr.php';
    $obj = new AdmissionsMgr();
    $course_batch_details = $obj->selAdmissions($_POST['school_name'],$_POST['class_name']);
    $str = array();    
    while($row = $course_batch_details->fetch()){
        $arr = array(
            'class_name' => $row['class_name'], 
            'first_name' => $row['first_name'], 
            'middle_name' => $row['middle_name'],             
            'last_name' => $row['last_name'],             
            'dob' => $row['dob'],             
            'gender' => $row['gender'], 
            'category' => $row['category'], 
            'cast' => $row['cast'],             
            'nationality' => $row['nationality'],             
            'student_samgraid' => $row['student_samgraid'],             
            'aadhar_no' => $row['aadhar_no'], 
            'family_samgraid' => $row['family_samgraid'], 
            'contact' => $row['contact'],             
            'email' => $row['email'],             
            'school_name' => $row['school_name'],             
            'address' => $row['address'], 
            'locality' => $row['locality'], 
            'village' => $row['village'],             
            'grampanchayat' => $row['grampanchayat'],             
            'nagarpanchayat' => $row['nagarpanchayat'],             
            'district' => $row['district'], 
            'pincode' => $row['pincode'], 
            'state' => $row['state'],             
            'image' => $row['image'],             
            'mothers_name' => $row['mothers_name'],             
            'mothers_occupation' => $row['mothers_occupation'],             
            'mothers_contact' => $row['mothers_contact'],             
            'fathers_name' => $row['fathers_name'],             
            'fathers_qualifications' => $row['fathers_qualifications'],             
            'fathers_occupation' => $row['fathers_occupation'],             
            'fathers_contact' => $row['fathers_contact'],             
            'fathers_emailid' => $row['fathers_emailid'],             
            'local_guardian' => $row['local_guardian'],             
            'local_guardian_contact' => $row['local_guardian_contact'],             
            'local_guardian_occupation' => $row['local_guardian_occupation'],             
            'last_school_attended' => $row['last_school_attended'],             
            'last_school_scholar' => $row['last_school_scholar'],             
            'last_class' => $row['last_class'],             
            'passed_year' => $row['passed_year'],             
            'passed_board' => $row['passed_board'],             
            'declaration' => $row['declaration'],             
            'datetime' => $row['datetime']            
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>