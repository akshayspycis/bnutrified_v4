<?php
     include_once '../models/DemoClass.php';
    include_once '../managers/DemoClassMgr.php';
    $obj = new DemoClassMgr();
    $demo_class = $obj->selDemoClass();
    $str = array();    
    while($row = $demo_class->fetch()){
            $arr = array(
            'demo_class_id' => $row['demo_class_id'], 
            'demo_class' => $row['demo_class'], 
            'status' => $row['status']
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>