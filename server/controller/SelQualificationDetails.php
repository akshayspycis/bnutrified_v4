<?php
     include_once '../models/QualificationDetails.php';
    include_once '../managers/QualificationDetailsMgr.php';
    $obj = new QualificationDetailsMgr();
    $course_batch_details = $obj->selQualificationDetails($_POST['course_registration_id']);
    $str = array();    
    while($row = $course_batch_details->fetch()){
        $arr = array(
            'class' => $row['class'], 
            'school_college' => $row['school_college'], 
            'board_university' => $row['board_university'],             
            'year_of_passing' => $row['year_of_passing'],             
            'percentage' => $row['percentage']
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>