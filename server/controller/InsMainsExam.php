<?php
    include_once '../models/MainsExam.php'; 
    include_once '../managers/MainsExamMgr.php'; 
    $mainsexam = new MainsExam();
    $mainsexam->setExam_id($_POST["selExamName"]);
    $mainsexam->setExam_sub_cat_id($_POST["selExamSubCat"]);
    $mainsexam->setExam_phase_id($_POST["selExamPhase"]);
    $mainsexam->setExam_section_id($_POST["selExamSection"]);
    $mainsexam->setNo_of_questions($_POST["no_of_questions"]);
    $mainsexam->setMax_marks($_POST["max_marks"]);
    $mainsexam->setDuration($_POST["duration"]);
    $mainsexam->setGen($_POST["gen"]);
    $mainsexam->setObc($_POST["obc"]);
    $mainsexam->setSc($_POST["sc"]);
    $mainsexam->setSt($_POST["st"]);
    $mainsexam->setPh($_POST["ph"]);
    $mainsexam->setVh($_POST["vh"]);
    $mainsexam->setYear($_POST["year"]);
    
    $mainsexamMgr = new MainsExamMgr();
    if($mainsexamMgr->insMainsExam($mainsexam)) {
        echo 'Mains Exams inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>


