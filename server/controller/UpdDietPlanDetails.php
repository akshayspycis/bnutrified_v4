 <?php
    include_once '../models/DietPlanDetails.php';
    include_once '../managers/DietPlanDetailsMgr.php';
    $diet_plan_details = new DietPlanDetails();    
    $diet_plan_details->setDiet_plan_details_id($_POST["diet_plan_details_id"]);
    $diet_plan_details->setPlan($_POST["editcate_plan"]);
    $diet_plan_details->setPrice($_POST["editcate_price"]);
    $diet_plan_details->setLink($_POST["editcate_link"]);
    $diet_plan_detailsMgr = new DietPlanDetailsMgr();    
    if ($diet_plan_detailsMgr->updateDietPlanDetails($diet_plan_details)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>