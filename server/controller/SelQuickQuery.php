<?php
     include_once '../models/QuickQuery.php';
     include_once '../managers/QuickQueryMgr.php';
    $obj = new QuickQueryMgr();
    $quick_query = $obj->selQuickQuery();
    $str = array();    
    while($row = $quick_query->fetch()){
            $arr = array(
            'id' => $row['id'], 
            'name' => $row['name'], 
            'email' => $row['email'],             
            'contact' => $row['contact'],             
            'location' => $row['location'],             
            'preferred_time' => $row['preferred_time'],             
            'date' => $row['date']    
       );
        array_push($str, $arr); 
    }
    
    echo json_encode($str);
?>