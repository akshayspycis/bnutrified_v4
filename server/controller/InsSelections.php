<?php
    include_once '../models/Selections.php'; 
    include_once '../managers/SelectionsMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $selections = new Selections();
    $selections->setName($_POST["name"]);
    $selections->setPercent($_POST["percent"]);  
    $selections->setClass($_POST["class"]);  
    $selections->setSchool_id($_POST['school_id']);
    $upload_dir = "upload/";
    $img = $_POST["image"];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir . time() . ".png";
    $success = file_put_contents($file, $data);
    $selections->setImage($file);   
    $selections->setDate($date->format('D, d M Y'));   
    $selectionsMgr = new SelectionsMgr();
    if ($selectionsMgr->insSelections($selections)) {
        echo 'Data inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>