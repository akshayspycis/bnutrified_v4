 <?php
    include_once '../models/Noticeboard.php';
    include_once '../managers/NoticeboardMgr.php';
    $noticeboard = new Noticeboard();    
    $noticeboard->setId($_POST["id"]);
    $noticeboard->setHeading($_POST["heading"]);
    $noticeboard->setContent($_POST["content"]);
    $noticeboardMgr = new NoticeboardMgr();    
    if($noticeboardMgr->updateNoticeboard($noticeboard)){
        echo 'Your data is updated successfully';
    }else {
        echo 'Error';
    }      
?>