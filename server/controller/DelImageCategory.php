<?php
    require_once '../managers/ImageCategoryMgr.php';
    
    $c_id = $_POST["c_id"];
    $categoryMgr = new ImageCategoryMgr();
    
    if ($categoryMgr->delImageCategory($c_id)) {
        echo 'Category Deleted Successfully.';
    } else {
        echo 'Error';
    }    
?>
    