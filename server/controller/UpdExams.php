 <?php
    include_once '../models/Exams.php';
    include_once '../managers/ExamsMgr.php';
    $exams = new Exams();    
    $exams->setExam_id($_POST["exam_id"]);
    $exams->setExam_name($_POST["exam_name"]);
    
    $examsMgr = new ExamsMgr();    
    if($examsMgr->updateExams($exams)) {
        echo 'Your data is updated successfully';
    } else {
        echo 'Error';
    }      
    
?>
