<?php
    include_once '../models/ExamsSubCategory.php';
    include_once '../managers/ExamsSubCategoryMgr.php';
        $obj = new ExamsSubCategoryMgr();
        
        $exams = $obj->selExamsSubCategory();
        $str = array();    
        while($row = $exams->fetch()){
            $arr = array(
                'exam_sub_cat_id' => $row['exam_sub_cat_id'], 
                'exam_sub_cat_name' => $row['exam_sub_cat_name'],
                'exam_name' => $row['exam_name'],
                );
            array_push($str, $arr); 
        }
        
    echo json_encode($str);
?>



