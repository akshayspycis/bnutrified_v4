<?php
     include_once '../models/DietCategoryDetails.php';
    include_once '../managers/DietCategoryDetailsMgr.php';
    $obj = new DietCategoryDetailsMgr();
    $diet_category_details = $obj->selDietCategoryDetails();
    $str = array();    
    while($row = $diet_category_details->fetch()){
            $arr = array(
            'diet_category_details_id' => $row['diet_category_details_id'], 
            'diet_category' => $row['diet_category']
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>