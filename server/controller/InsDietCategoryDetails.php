<?php
    include_once '../models/DietCategoryDetails.php'; 
    include_once '../managers/DietCategoryDetailsMgr.php'; 
    $diet_category_details = new DietCategoryDetails();
    $diet_category_details->setDiet_category($_POST["diet_category"]);
    $diet_category_detailsMgr = new DietCategoryDetailsMgr();
    if ($diet_category_detailsMgr->insDietCategoryDetails($diet_category_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>