<?php
    include_once '../models/ForumCategoryDetails.php'; 
    include_once '../managers/ForumCategoryDetailsMgr.php'; 
        $forum_category_details = new ForumCategoryDetails();
    $forum_category_details->setForum_category($_POST["forum_category"]);
    $forum_category_details->setCategory_url(str_replace(' ', '-', strtolower($_POST["forum_category"])));
    $forum_category_detailsMgr = new ForumCategoryDetailsMgr();
    
    if ($forum_category_detailsMgr->insForumCategoryDetails($forum_category_details)) {
        echo 'Query inserted Successfully.';
    } else {
        echo 'Error';
    }
?>