<?php
    include_once '../models/Selections.php';
    include_once '../managers/SelectionsMgr.php';
    $selections = new Selections(); 
    $selections->setId($_POST['id']);
    $upload_dir = "upload/";
    $img = $_POST['image'];
    $img = str_replace('data:image/jpeg;base64,', '', $img);
    $img = str_replace(' ', '+', $img);
    $data = base64_decode($img);
    $file = $upload_dir . time() . ".png";
    $success = file_put_contents($file, $data);
    $selections->setImage($file);
    $selectionsMgr = new SelectionsMgr(); 
    if ($selectionsMgr->updSelectionsImg($selections)) {
     echo 'true';
    } else {
        echo 'Manager Error Found';        
    }
?>
