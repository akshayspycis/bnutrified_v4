<?php
    session_start();
    include_once '../models/ThoughtofDay.php'; 
    include_once '../managers/ThoughtofDayMgr.php'; 
    $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
    $thoughtofday = new ThoughtofDay();
    $thoughtofday->setContent($_POST["content"]);
    $thoughtofday->setAuthor($_POST["author"]);  
    $thoughtofday->setUser_registration_for_demo_id($_SESSION['user_id']);   
    $thoughtofday->setPosteddate($date->format('D, d M Y'));   
    $thoughtofdayMgr = new ThoughtofDayMgr();
    if ($thoughtofdayMgr->insThoughtofDay($thoughtofday)) {
        echo 'ThoughtofDay inserted Successfully.';
        
    } else {
        echo 'Error';
    }
?>