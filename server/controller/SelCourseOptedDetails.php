<?php
     include_once '../models/CourseOptedDetails.php';
    include_once '../managers/CourseOptedDetailsMgr.php';
    $obj = new CourseOptedDetailsMgr();
    $course_batch_details = $obj->selCourseOptedDetails($_POST['course_registration_id']);
    $str = array();    
    while($row = $course_batch_details->fetch()){
        $arr = array(
            'course_po_mt_and_clerk' => $row['course_po_mt_and_clerk'], 
            'advance_comm_english' => $row['advance_comm_english'], 
            'ssc_and_railways' => $row['ssc_and_railways'],             
            'english_for_competitive_exams' => $row['english_for_competitive_exams'],             
            'personal_interviews' => $row['personal_interviews'],             
            'mba_entrance_exam' => $row['mba_entrance_exam']            
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>