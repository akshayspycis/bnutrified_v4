<?php
     include_once '../models/DietPlanDetails.php';
    include_once '../managers/DietPlanDetailsMgr.php';
    $obj = new DietPlanDetailsMgr();
    $diet_plan_details = $obj->selDietPlanDetails($_POST['diet_category_details_id']);
    $str = array();    
    while($row = $diet_plan_details->fetch()){
       $arr = array(
            'diet_plan_details_id' => $row['diet_plan_details_id'], 
            'diet_category_details_id' => $row['diet_category_details_id'],
            'plan' => $row['plan'],
            'price' => $row['price'],
            'link' => $row['link'],
       );
        array_push($str, $arr); 
    }
    echo json_encode($str);
?>