<?php
include_once '../models/Downloadsection.php'; 
include_once '../managers/DownloadsectionMgr.php';
$res = array(); 
 foreach ($_FILES["files"]["error"] as $key => $error)
   {
    if ($error == UPLOAD_ERR_OK)
    {
        $name = $_FILES["files"]["name"][$key];
        if(file_exists('pdf/'.$name))
        {
            unlink('pdf/'.$name);
        }
        $path = 'pdf/'.$name;
        move_uploaded_file( $_FILES["files"]["tmp_name"][$key], "pdf/" . $name);
        
             
            $date = new DateTime("now", new DateTimeZone("Asia/Kolkata"));
            $downloadsection = new Downloadsection();
            $downloadsection->setName($_POST["name"]);
            $downloadsection->setPath($path);
            $downloadsection->setDate($date->format('d M Y'));
            $downloadsectionMgr = new DownloadsectionMgr();
            if ($downloadsectionMgr->insDownloadsection($downloadsection)) {
            $success = 'Uploaded Successfully';
            $res[] = $name;
//            $res[] = $path;
            $res[] = $success;
            } else {
            echo 'Error';
            }
        
        
    }
    else
    {
        echo json_encode(array('res'=>FALSE,'data'=>'Error uploading '.$name));
        exit(1);
    }
}
echo json_encode(array('res'=>TRUE,'data'=>$res));
exit(0);

