<?php
    class CourseRegistration {

        private $course_registration_id;
        private $dob;
        private $user_registration_for_demo_id;
        private $additional_qualification;
        private $computer_literacy;
        private $address_of_institiute_or_company;
        private $relative_name;
        private $relative_occupation;
        private $applicant_occupation;
        private $school;
        private $college;
        private $relative_contact_no;
        private $referal_name;
        private $referal_contact_no;
        public function getCourse_registration_id() {
            return $this->course_registration_id;
        }

        public function getDob() {
            return $this->dob;
        }

        public function getUser_registration_for_demo_id() {
            return $this->user_registration_for_demo_id;
        }

        public function getAdditional_qualification() {
            return $this->additional_qualification;
        }

        public function getComputer_literacy() {
            return $this->computer_literacy;
        }

        public function getAddress_of_institiute_or_company() {
            return $this->address_of_institiute_or_company;
        }

        public function getRelative_name() {
            return $this->relative_name;
        }

        public function getRelative_occupation() {
            return $this->relative_occupation;
        }

        public function getApplicant_occupation() {
            return $this->applicant_occupation;
        }

        public function getSchool() {
            return $this->school;
        }

        public function getCollege() {
            return $this->college;
        }

        public function getRelative_contact_no() {
            return $this->relative_contact_no;
        }

        public function getReferal_name() {
            return $this->referal_name;
        }

        public function getReferal_contact_no() {
            return $this->referal_contact_no;
        }

        public function setCourse_registration_id($course_registration_id) {
            $this->course_registration_id = $course_registration_id;
        }

        public function setDob($dob) {
            $this->dob = $dob;
        }

        public function setUser_registration_for_demo_id($user_registration_for_demo_id) {
            $this->user_registration_for_demo_id = $user_registration_for_demo_id;
        }

        public function setAdditional_qualification($additional_qualification) {
            $this->additional_qualification = $additional_qualification;
        }

        public function setComputer_literacy($computer_literacy) {
            $this->computer_literacy = $computer_literacy;
        }

        public function setAddress_of_institiute_or_company($address_of_institiute_or_company) {
            $this->address_of_institiute_or_company = $address_of_institiute_or_company;
        }

        public function setRelative_name($relative_name) {
            $this->relative_name = $relative_name;
        }

        public function setRelative_occupation($relative_occupation) {
            $this->relative_occupation = $relative_occupation;
        }

        public function setApplicant_occupation($applicant_occupation) {
            $this->applicant_occupation = $applicant_occupation;
        }

        public function setSchool($school) {
            $this->school = $school;
        }

        public function setCollege($college) {
            $this->college = $college;
        }

        public function setRelative_contact_no($relative_contact_no) {
            $this->relative_contact_no = $relative_contact_no;
        }

        public function setReferal_name($referal_name) {
            $this->referal_name = $referal_name;
        }

        public function setReferal_contact_no($referal_contact_no) {
            $this->referal_contact_no = $referal_contact_no;
        }

    }


