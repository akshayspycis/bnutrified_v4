<?php
    class FoodDetails{

        private $food_details_id;
        private $title;
        private $pic;
        private $discription;
        private $price;
        private $status;
        private $food_category_details_id;
        function getFood_details_id() {
            return $this->food_details_id;
        }

        function getTitle() {
            return $this->title;
        }

        function getPic() {
            return $this->pic;
        }

        function getDiscription() {
            return $this->discription;
        }

        function getPrice() {
            return $this->price;
        }

        function getStatus() {
            return $this->status;
        }

        function getFood_category_details_id() {
            return $this->food_category_details_id;
        }

        function setFood_details_id($food_details_id) {
            $this->food_details_id = $food_details_id;
        }

        function setTitle($title) {
            $this->title = $title;
        }

        function setPic($pic) {
            $this->pic = $pic;
        }

        function setDiscription($discription) {
            $this->discription = $discription;
        }

        function setPrice($price) {
            $this->price = $price;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setFood_category_details_id($food_category_details_id) {
            $this->food_category_details_id = $food_category_details_id;
        }


    }


