<?php
    class ForumPostDetails{
        
        private $forum_post_details_id;
        private $forum_topic_details_id;
        private $forum_category_details_id;
        private $date;
        private $user_id;
        private $user_name;
        private $post;
        private $status;
        
        public function getForum_post_details_id() {
            return $this->forum_post_details_id;
        }

        public function getForum_topic_details_id() {
            return $this->forum_topic_details_id;
        }

        public function getForum_category_details_id() {
            return $this->forum_category_details_id;
        }

        public function getDate() {
            return $this->date;
        }

        public function getUser_id() {
            return $this->user_id;
        }

        public function getUser_name() {
            return $this->user_name;
        }

        public function getPost() {
            return $this->post;
        }

        public function getStatus() {
            return $this->status;
        }

        public function setForum_post_details_id($forum_post_details_id) {
            $this->forum_post_details_id = $forum_post_details_id;
        }

        public function setForum_topic_details_id($forum_topic_details_id) {
            $this->forum_topic_details_id = $forum_topic_details_id;
        }

        public function setForum_category_details_id($forum_category_details_id) {
            $this->forum_category_details_id = $forum_category_details_id;
        }

        public function setDate($date) {
            $this->date = $date;
        }

        public function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        public function setUser_name($user_name) {
            $this->user_name = $user_name;
        }

        public function setPost($post) {
            $this->post = $post;
        }

        public function setStatus($status) {
            $this->status = $status;
        }




        }



