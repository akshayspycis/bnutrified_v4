<?php
    class BlogDetails{
        
        private $blog_details_id;
        private $user_id;
        private $title;
        private $short_description;
        private $long_description;
        private $date;
        private $category_id;
        private $status;
        private $pic;
        private $fb;
        private $tw;
        private $link;
        private $a_pic;
        
        function getBlog_details_id() {
            return $this->blog_details_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getTitle() {
            return $this->title;
        }

        function getShort_description() {
            return $this->short_description;
        }

        function getLong_description() {
            return $this->long_description;
        }

        function getDate() {
            return $this->date;
        }

        function getCategory_id() {
            return $this->category_id;
        }

        function getStatus() {
            return $this->status;
        }

        function getPic() {
            return $this->pic;
        }

        function getFb() {
            return $this->fb;
        }

        function getTw() {
            return $this->tw;
        }

        function getLink() {
            return $this->link;
        }

        function getA_pic() {
            return $this->a_pic;
        }

        function setBlog_details_id($blog_details_id) {
            $this->blog_details_id = $blog_details_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setTitle($title) {
            $this->title = $title;
        }

        function setShort_description($short_description) {
            $this->short_description = $short_description;
        }

        function setLong_description($long_description) {
            $this->long_description = $long_description;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setCategory_id($category_id) {
            $this->category_id = $category_id;
        }

        function setStatus($status) {
            $this->status = $status;
        }

        function setPic($pic) {
            $this->pic = $pic;
        }

        function setFb($fb) {
            $this->fb = $fb;
        }

        function setTw($tw) {
            $this->tw = $tw;
        }

        function setLink($link) {
            $this->link = $link;
        }

        function setA_pic($a_pic) {
            $this->a_pic = $a_pic;
        }

        }



