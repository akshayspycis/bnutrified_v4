<?php
    class ExamsPhases{
        private $exam_phase_id;
        private $exam_phase_name;
        private $exam_id;
        private $exam_sub_cat_id;
        
        function getExam_phase_id() {
            return $this->exam_phase_id;
        }

        function getExam_phase_name() {
            return $this->exam_phase_name;
        }

        function getExam_id() {
            return $this->exam_id;
        }

        function getExam_sub_cat_id() {
            return $this->exam_sub_cat_id;
        }

        function setExam_phase_id($exam_phase_id) {
            $this->exam_phase_id = $exam_phase_id;
        }

        function setExam_phase_name($exam_phase_name) {
            $this->exam_phase_name = $exam_phase_name;
        }

        function setExam_id($exam_id) {
            $this->exam_id = $exam_id;
        }

        function setExam_sub_cat_id($exam_sub_cat_id) {
            $this->exam_sub_cat_id = $exam_sub_cat_id;
        }


}
?>
