<?php
    class StepDetails{
        private $step_details_id;
        private $gender;
        private $goal;
        private $fitness_activity;
        private $name;
        private $email;
        private $contact;
        private $location;
        private $dob;
        private $date;
        private $height;
        private $weight;
        
        function getStep_details_id() {
            return $this->step_details_id;
        }

        function getGender() {
            return $this->gender;
        }

        function getGoal() {
            return $this->goal;
        }

        function getFitness_activity() {
            return $this->fitness_activity;
        }

        function getName() {
            return $this->name;
        }

        function getEmail() {
            return $this->email;
        }

        function getContact() {
            return $this->contact;
        }

        function getLocation() {
            return $this->location;
        }

        function getDob() {
            return $this->dob;
        }

        function getDate() {
            return $this->date;
        }

        function getHeight() {
            return $this->height;
        }

        function getWeight() {
            return $this->weight;
        }

        function setStep_details_id($step_details_id) {
            $this->step_details_id = $step_details_id;
        }

        function setGender($gender) {
            $this->gender = $gender;
        }

        function setGoal($goal) {
            $this->goal = $goal;
        }

        function setFitness_activity($fitness_activity) {
            $this->fitness_activity = $fitness_activity;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setContact($contact) {
            $this->contact = $contact;
        }

        function setLocation($location) {
            $this->location = $location;
        }

        function setDob($dob) {
            $this->dob = $dob;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setHeight($height) {
            $this->height = $height;
        }

        function setWeight($weight) {
            $this->weight = $weight;
        }


        
}
?>