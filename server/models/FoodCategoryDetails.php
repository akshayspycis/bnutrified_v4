<?php
    class FoodCategoryDetails{
        private $food_category_details_id;
        private $food_category;
        function getFood_category_details_id() {
            return $this->food_category_details_id;
        }
        function getFood_category() {
            return $this->food_category;
        }
        function setFood_category_details_id($food_category_details_id) {
            $this->food_category_details_id = $food_category_details_id;
        }
        function setFood_category($food_category) {
            $this->food_category = $food_category;
        }
    }



