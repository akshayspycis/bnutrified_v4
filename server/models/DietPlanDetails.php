<?php
    class DietPlanDetails{
        private $diet_plan_details_id;
        private $diet_category_details_id;
        private $plan;
        private $price;
        private $link;
        function getDiet_plan_details_id() {
            return $this->diet_plan_details_id;
        }

        function getDiet_category_details_id() {
            return $this->diet_category_details_id;
        }

        function getPlan() {
            return $this->plan;
        }

        function getPrice() {
            return $this->price;
        }

        function getLink() {
            return $this->link;
        }

        function setDiet_plan_details_id($diet_plan_details_id) {
            $this->diet_plan_details_id = $diet_plan_details_id;
        }

        function setDiet_category_details_id($diet_category_details_id) {
            $this->diet_category_details_id = $diet_category_details_id;
        }

        function setPlan($plan) {
            $this->plan = $plan;
        }

        function setPrice($price) {
            $this->price = $price;
        }

        function setLink($link) {
            $this->link = $link;
        }


}



