<?php
    class ForumTopicDetails{
        
        private $forum_topic_details_id;
        private $forum_category_details_id;
        private $date;
        private $topic_name;
        private $topic_url;
         
        public function getForum_topic_details_id() {
            return $this->forum_topic_details_id;
        }

        public function getForum_category_details_id() {
            return $this->forum_category_details_id;
        }

        public function getDate() {
            return $this->date;
        }

        public function getTopic_name() {
            return $this->topic_name;
        }

        public function getTopic_url() {
            return $this->topic_url;
        }

        public function setForum_topic_details_id($forum_topic_details_id) {
            $this->forum_topic_details_id = $forum_topic_details_id;
        }

        public function setForum_category_details_id($forum_category_details_id) {
            $this->forum_category_details_id = $forum_category_details_id;
        }

        public function setDate($date) {
            $this->date = $date;
        }

        public function setTopic_name($topic_name) {
            $this->topic_name = $topic_name;
        }

        public function setTopic_url($topic_url) {
            $this->topic_url = $topic_url;
        }

        
        }



