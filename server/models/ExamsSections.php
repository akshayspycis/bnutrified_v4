<?php
    class ExamsSections{
        private $exam_section_id;
        private $exam_section_name;
        private $exam_phase_id;
        private $exam_sub_cat_id;
        private $exam_id;
        
        function getExam_section_id() {
            return $this->exam_section_id;
        }

        function getExam_section_name() {
            return $this->exam_section_name;
        }

        function getExam_phase_id() {
            return $this->exam_phase_id;
        }

        function getExam_sub_cat_id() {
            return $this->exam_sub_cat_id;
        }

        function getExam_id() {
            return $this->exam_id;
        }

        function setExam_section_id($exam_section_id) {
            $this->exam_section_id = $exam_section_id;
        }

        function setExam_section_name($exam_section_name) {
            $this->exam_section_name = $exam_section_name;
        }

        function setExam_phase_id($exam_phase_id) {
            $this->exam_phase_id = $exam_phase_id;
        }

        function setExam_sub_cat_id($exam_sub_cat_id) {
            $this->exam_sub_cat_id = $exam_sub_cat_id;
        }

        function setExam_id($exam_id) {
            $this->exam_id = $exam_id;
        }

}
?>
