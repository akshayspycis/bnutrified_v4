<?php
    class BearWitness{
        private $bear_witness_id;
        private $attestation;
        private $pic_path;
        private $course_registration_id;
        
        public function getBear_witness_id() {
            return $this->bear_witness_id;
        }

        public function getAttestation() {
            return $this->attestation;
        }

        public function getPic_path() {
            return $this->pic_path;
        }

        public function getCourse_registration_id() {
            return $this->course_registration_id;
        }

        public function setBear_witness_id($bear_witness_id) {
            $this->bear_witness_id = $bear_witness_id;
        }

        public function setAttestation($attestation) {
            $this->attestation = $attestation;
        }

        public function setPic_path($pic_path) {
            $this->pic_path = $pic_path;
        }

        public function setCourse_registration_id($course_registration_id) {
            $this->course_registration_id = $course_registration_id;
        }


    }


