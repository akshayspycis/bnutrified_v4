<?php
    class ApplyJob{
        
        private $career_id;
        private $name;
        private $email;
        private $contact_no;
        private $message;
        private $pdf;
        private $date;
        
        function getCareer_id() {
            return $this->career_id;
        }

        function getName() {
            return $this->name;
        }

        function getEmail() {
            return $this->email;
        }

        function getContact_no() {
            return $this->contact_no;
        }

        function getMessage() {
            return $this->message;
        }

        function getPdf() {
            return $this->pdf;
        }

        function getDate() {
            return $this->date;
        }

        function setCareer_id($career_id) {
            $this->career_id = $career_id;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setContact_no($contact_no) {
            $this->contact_no = $contact_no;
        }

        function setMessage($message) {
            $this->message = $message;
        }

        function setPdf($pdf) {
            $this->pdf = $pdf;
        }

        function setDate($date) {
            $this->date = $date;
        }

  
        }



