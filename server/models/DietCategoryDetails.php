<?php
    class DietCategoryDetails{
        
        private $diet_category_details_id;
        private $diet_category;
        
        function getDiet_category_details_id() {
            return $this->diet_category_details_id;
        }

        function getDiet_category() {
            return $this->diet_category;
        }

        function setDiet_category_details_id($diet_category_details_id) {
            $this->diet_category_details_id = $diet_category_details_id;
        }

        function setDiet_category($diet_category) {
            $this->diet_category = $diet_category;
        }


}



