<?php
    class Admissions{
        private $admissions_id;
        private $class_name;
        private $first_name;
        private $middle_name;
        private $last_name;
        private $dob;
        private $gender;
        private $category;
        private $cast;
        private $nationality;
        private $student_samgraid;
        private $aadhar_no;
        private $family_samgraid;
        private $contact;
        private $email;
        private $school_name;
        private $address;
        private $locality;
        private $village;
        private $grampanchayat;
        private $nagarpanchayat;
        private $district;
        private $pincode;
        private $state;
        private $image;
        private $mothers_name;
        private $mothers_qualifications;
        private $mothers_occupation;
        private $mothers_contact;
        private $fathers_name;
        private $fathers_qualifications;
        private $fathers_occupation;
        private $fathers_contact;
        private $fathers_emailid;
        private $local_guardian;
        private $local_guardian_contact;
        private $local_guardian_occupation;
        private $last_school_attended;
        private $last_school_scholar;
        private $last_class;
        private $passed_year;
        private $passed_board;
        private $declaration;
        private $datetime;
        
        function getAdmissions_id() {
            return $this->admissions_id;
        }

        function getClass_name() {
            return $this->class_name;
        }

        function getFirst_name() {
            return $this->first_name;
        }

        function getMiddle_name() {
            return $this->middle_name;
        }

        function getLast_name() {
            return $this->last_name;
        }

        function getDob() {
            return $this->dob;
        }

        function getGender() {
            return $this->gender;
        }

        function getCategory() {
            return $this->category;
        }

        function getCast() {
            return $this->cast;
        }

        function getNationality() {
            return $this->nationality;
        }

        function getStudent_samgraid() {
            return $this->student_samgraid;
        }

        function getAadhar_no() {
            return $this->aadhar_no;
        }

        function getFamily_samgraid() {
            return $this->family_samgraid;
        }

        function getContact() {
            return $this->contact;
        }

        function getEmail() {
            return $this->email;
        }

        function getSchool_name() {
            return $this->school_name;
        }

        function getAddress() {
            return $this->address;
        }

        function getLocality() {
            return $this->locality;
        }

        function getVillage() {
            return $this->village;
        }

        function getGrampanchayat() {
            return $this->grampanchayat;
        }

        function getNagarpanchayat() {
            return $this->nagarpanchayat;
        }

        function getDistrict() {
            return $this->district;
        }

        function getPincode() {
            return $this->pincode;
        }

        function getState() {
            return $this->state;
        }

        function getImage() {
            return $this->image;
        }

        function getMothers_name() {
            return $this->mothers_name;
        }

        function getMothers_qualifications() {
            return $this->mothers_qualifications;
        }

        function getMothers_occupation() {
            return $this->mothers_occupation;
        }

        function getMothers_contact() {
            return $this->mothers_contact;
        }

        function getFathers_name() {
            return $this->fathers_name;
        }

        function getFathers_qualifications() {
            return $this->fathers_qualifications;
        }

        function getFathers_occupation() {
            return $this->fathers_occupation;
        }

        function getFathers_contact() {
            return $this->fathers_contact;
        }

        function getFathers_emailid() {
            return $this->fathers_emailid;
        }

        function getLocal_guardian() {
            return $this->local_guardian;
        }

        function getLocal_guardian_contact() {
            return $this->local_guardian_contact;
        }

        function getLocal_guardian_occupation() {
            return $this->local_guardian_occupation;
        }

        function getLast_school_attended() {
            return $this->last_school_attended;
        }

        function getLast_school_scholar() {
            return $this->last_school_scholar;
        }

        function getLast_class() {
            return $this->last_class;
        }

        function getPassed_year() {
            return $this->passed_year;
        }

        function getPassed_board() {
            return $this->passed_board;
        }

        function getDeclaration() {
            return $this->declaration;
        }

        function getDatetime() {
            return $this->datetime;
        }

        function setAdmissions_id($admissions_id) {
            $this->admissions_id = $admissions_id;
        }

        function setClass_name($class_name) {
            $this->class_name = $class_name;
        }

        function setFirst_name($first_name) {
            $this->first_name = $first_name;
        }

        function setMiddle_name($middle_name) {
            $this->middle_name = $middle_name;
        }

        function setLast_name($last_name) {
            $this->last_name = $last_name;
        }

        function setDob($dob) {
            $this->dob = $dob;
        }

        function setGender($gender) {
            $this->gender = $gender;
        }

        function setCategory($category) {
            $this->category = $category;
        }

        function setCast($cast) {
            $this->cast = $cast;
        }

        function setNationality($nationality) {
            $this->nationality = $nationality;
        }

        function setStudent_samgraid($student_samgraid) {
            $this->student_samgraid = $student_samgraid;
        }

        function setAadhar_no($aadhar_no) {
            $this->aadhar_no = $aadhar_no;
        }

        function setFamily_samgraid($family_samgraid) {
            $this->family_samgraid = $family_samgraid;
        }

        function setContact($contact) {
            $this->contact = $contact;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setSchool_name($school_name) {
            $this->school_name = $school_name;
        }

        function setAddress($address) {
            $this->address = $address;
        }

        function setLocality($locality) {
            $this->locality = $locality;
        }

        function setVillage($village) {
            $this->village = $village;
        }

        function setGrampanchayat($grampanchayat) {
            $this->grampanchayat = $grampanchayat;
        }

        function setNagarpanchayat($nagarpanchayat) {
            $this->nagarpanchayat = $nagarpanchayat;
        }

        function setDistrict($district) {
            $this->district = $district;
        }

        function setPincode($pincode) {
            $this->pincode = $pincode;
        }

        function setState($state) {
            $this->state = $state;
        }

        function setImage($image) {
            $this->image = $image;
        }

        function setMothers_name($mothers_name) {
            $this->mothers_name = $mothers_name;
        }

        function setMothers_qualifications($mothers_qualifications) {
            $this->mothers_qualifications = $mothers_qualifications;
        }

        function setMothers_occupation($mothers_occupation) {
            $this->mothers_occupation = $mothers_occupation;
        }

        function setMothers_contact($mothers_contact) {
            $this->mothers_contact = $mothers_contact;
        }

        function setFathers_name($fathers_name) {
            $this->fathers_name = $fathers_name;
        }

        function setFathers_qualifications($fathers_qualifications) {
            $this->fathers_qualifications = $fathers_qualifications;
        }

        function setFathers_occupation($fathers_occupation) {
            $this->fathers_occupation = $fathers_occupation;
        }

        function setFathers_contact($fathers_contact) {
            $this->fathers_contact = $fathers_contact;
        }

        function setFathers_emailid($fathers_emailid) {
            $this->fathers_emailid = $fathers_emailid;
        }

        function setLocal_guardian($local_guardian) {
            $this->local_guardian = $local_guardian;
        }

        function setLocal_guardian_contact($local_guardian_contact) {
            $this->local_guardian_contact = $local_guardian_contact;
        }

        function setLocal_guardian_occupation($local_guardian_occupation) {
            $this->local_guardian_occupation = $local_guardian_occupation;
        }

        function setLast_school_attended($last_school_attended) {
            $this->last_school_attended = $last_school_attended;
        }

        function setLast_school_scholar($last_school_scholar) {
            $this->last_school_scholar = $last_school_scholar;
        }

        function setLast_class($last_class) {
            $this->last_class = $last_class;
        }

        function setPassed_year($passed_year) {
            $this->passed_year = $passed_year;
        }

        function setPassed_board($passed_board) {
            $this->passed_board = $passed_board;
        }

        function setDeclaration($declaration) {
            $this->declaration = $declaration;
        }

        function setDatetime($datetime) {
            $this->datetime = $datetime;
        }


        
        


}
?>