<?php
    class ReferDetails{
        private $refer_details_id;
        private $user_id;
        private $user_name;
        private $email;
        private $contact_no;
        private $date;
        function getRefer_details_id() {
            return $this->refer_details_id;
        }

        function getUser_id() {
            return $this->user_id;
        }

        function getUser_name() {
            return $this->user_name;
        }

        function getEmail() {
            return $this->email;
        }

        function getContact_no() {
            return $this->contact_no;
        }

        function getDate() {
            return $this->date;
        }

        function setRefer_details_id($refer_details_id) {
            $this->refer_details_id = $refer_details_id;
        }

        function setUser_id($user_id) {
            $this->user_id = $user_id;
        }

        function setUser_name($user_name) {
            $this->user_name = $user_name;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setContact_no($contact_no) {
            $this->contact_no = $contact_no;
        }

        function setDate($date) {
            $this->date = $date;
        }

    }



