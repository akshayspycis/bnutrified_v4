<?php
    class Selections{
        private $id;
        private $name;
        private $percent;
        private $class;
        private $school_id;
        private $image;
        private $date;
        
        function getId() {
            return $this->id;
        }

        function getName() {
            return $this->name;
        }

        function getPercent() {
            return $this->percent;
        }

        function getClass() {
            return $this->class;
        }

        function getSchool_id() {
            return $this->school_id;
        }

        function getImage() {
            return $this->image;
        }

        function getDate() {
            return $this->date;
        }

        function setId($id) {
            $this->id = $id;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setPercent($percent) {
            $this->percent = $percent;
        }

        function setClass($class) {
            $this->class = $class;
        }

        function setSchool_id($school_id) {
            $this->school_id = $school_id;
        }

        function setImage($image) {
            $this->image = $image;
        }

        function setDate($date) {
            $this->date = $date;
        }


       
    }




