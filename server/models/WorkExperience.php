<?php
    class WorkExperience{
        private $work_experience_id;
        private $organization;
        private $time;
        private $designation;
        private $course_registration_id;
        
        function getWork_experience_id() {
            return $this->work_experience_id;
        }

        function getOrganization() {
            return $this->organization;
        }

        function getTime() {
            return $this->time;
        }

        function getDesignation() {
            return $this->designation;
        }

        function getCourse_registration_id() {
            return $this->course_registration_id;
        }

        function setWork_experience_id($work_experience_id) {
            $this->work_experience_id = $work_experience_id;
        }

        function setOrganization($organization) {
            $this->organization = $organization;
        }

        function setTime($time) {
            $this->time = $time;
        }

        function setDesignation($designation) {
            $this->designation = $designation;
        }

        function setCourse_registration_id($course_registration_id) {
            $this->course_registration_id = $course_registration_id;
        }


        

    }


