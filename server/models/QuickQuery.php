<?php
    class QuickQuery{
        private $quick_query_id;
        private $name;
        private $email;
        private $contact;
        private $location;
        private $date;
        private $preferred_time;
        function getQuick_query_id() {
            return $this->quick_query_id;
        }

        function getName() {
            return $this->name;
        }

        function getEmail() {
            return $this->email;
        }

        function getContact() {
            return $this->contact;
        }

        function getLocation() {
            return $this->location;
        }

        function getDate() {
            return $this->date;
        }

        function getPreferred_time() {
            return $this->preferred_time;
        }

        function setQuick_query_id($quick_query_id) {
            $this->quick_query_id = $quick_query_id;
        }

        function setName($name) {
            $this->name = $name;
        }

        function setEmail($email) {
            $this->email = $email;
        }

        function setContact($contact) {
            $this->contact = $contact;
        }

        function setLocation($location) {
            $this->location = $location;
        }

        function setDate($date) {
            $this->date = $date;
        }

        function setPreferred_time($preferred_time) {
            $this->preferred_time = $preferred_time;
        }

        
    }



