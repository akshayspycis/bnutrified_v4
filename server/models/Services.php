<?php
    class Services{
        private $services_id;
        private $icon;
        private $title;
        private $discription;
        
        function getServices_id() {
            return $this->services_id;
        }

        function getIcon() {
            return $this->icon;
        }

        function getTitle() {
            return $this->title;
        }

        function getDiscription() {
            return $this->discription;
        }

        function setServices_id($services_id) {
            $this->services_id = $services_id;
        }

        function setIcon($icon) {
            $this->icon = $icon;
        }

        function setTitle($title) {
            $this->title = $title;
        }

        function setDiscription($discription) {
            $this->discription = $discription;
        }


    }


