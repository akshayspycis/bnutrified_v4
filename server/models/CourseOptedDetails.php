<?php
    class CourseOptedDetails{

        private $course_opted_details_id;
        private $course_po_mt_and_clerk;
        private $advance_comm_english;
        private $ssc_and_railways;
        private $english_for_competitive_exams;
        private $personal_interviews;
        private $mba_entrance_exam;
        private $course_registration_id;
        public function getCourse_opted_details_id() {
            return $this->course_opted_details_id;
        }

        public function getCourse_po_mt_and_clerk() {
            return $this->course_po_mt_and_clerk;
        }

        public function getAdvance_comm_english() {
            return $this->advance_comm_english;
        }

        public function getSsc_and_railways() {
            return $this->ssc_and_railways;
        }

        public function getEnglish_for_competitive_exams() {
            return $this->english_for_competitive_exams;
        }

        public function getPersonal_interviews() {
            return $this->personal_interviews;
        }

        public function getMba_entrance_exam() {
            return $this->mba_entrance_exam;
        }

        public function getCourse_registration_id() {
            return $this->course_registration_id;
        }

        public function setCourse_opted_details_id($course_opted_details_id) {
            $this->course_opted_details_id = $course_opted_details_id;
        }

        public function setCourse_po_mt_and_clerk($course_po_mt_and_clerk) {
            $this->course_po_mt_and_clerk = $course_po_mt_and_clerk;
        }

        public function setAdvance_comm_english($advance_comm_english) {
            $this->advance_comm_english = $advance_comm_english;
        }

        public function setSsc_and_railways($ssc_and_railways) {
            $this->ssc_and_railways = $ssc_and_railways;
        }

        public function setEnglish_for_competitive_exams($english_for_competitive_exams) {
            $this->english_for_competitive_exams = $english_for_competitive_exams;
        }

        public function setPersonal_interviews($personal_interviews) {
            $this->personal_interviews = $personal_interviews;
        }

        public function setMba_entrance_exam($mba_entrance_exam) {
            $this->mba_entrance_exam = $mba_entrance_exam;
        }

        public function setCourse_registration_id($course_registration_id) {
            $this->course_registration_id = $course_registration_id;
        }


    }


