$(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('#requestModal').on('shown.bs.modal', function(){
        $("#rmodalHeading").show();
        $("#quick-query").show();
        $("#id_or_call").show();
        $("#rbNumber").show();
        $("#show_message").hide();
        $("#para_dispaly").hide();
    });
    $('#quick-query').off() ;
    $('#quick-query').submit(function() { 
        if(this.name.value === "") {
                  $("#errorMsg").empty(); 
                  $("#errorMsg").append("Please fill all the field.") ;
            this.name.focus();
            return false;
        }
        else if(this.email.value === "" || !emailfilter.test(this.email.value)) {
                $("#errorMsg").empty(); 
                  $("#errorMsg").append("Please fill all the field.") ;
            this.email.focus();
            return false;
        }
        else if(this.contact.value == ""|| !contactnofilter.test(this.contact.value)) {
                $("#errorMsg").empty();  
                $("#errorMsg").append("Please fill all the field.") ;
            this.contact.focus();
            return false;
        }
        else if (this.location.value == "") {
                $("#errorMsg").empty();  
                $("#errorMsg").append("Please fill all the field.") ;
            this.location.focus();
            return false;
        }
        else if (this.preferred_time.value == "") {
                $("#errorMsg").empty();  
                $("#errorMsg").append("Please fill all the field.") ;
            this.location.focus();
            return false;
        }
        else {    
            $('#quick-query').find('#rbutton').attr("disabled", true); 
            $('#quick-query').find('#rbutton').html($("<img>").attr({'src':'images/fb_ty.gif'}))
            setTimeout(function (){
                $('#quick-query').each(function(){
                            this.reset();
                        });
                $("#rmodalHeading").hide();
                $("#quick-query").hide();
                $("#id_or_call").hide();
                $("#rbNumber").hide();
                $("#show_message").show();
                $("#para_dispaly").show();
                $("#show_message").html("Thank you for your time! This is your first step to a transformed you, we will get in touch with you shortly.");
                $('#quick-query').find('#rbutton').attr("disabled", false); 
                $('#quick-query').find('#rbutton').html("Submit");
                
            },500);
            $.ajax({
                type:"post",
                url:"server/controller/InsQuickQuery.php",
                data:$('#quick-query').serialize(),
                success: function(data){
                    if(data.trim()==="Success"){
                        $('#quick-query').each(function(){
                            this.reset();
                        });
                        $('#success_msg_demo').modal('hide');
                    }
                }
            });
        }
        closeNav();
        return false;
    });
});

