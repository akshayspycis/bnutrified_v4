    $(document).ready(function() {
    var emailfilter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    var contactnofilter = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
    $('#letsgo').click(function() { 
        if($("#step_details").find("#name").val() === "") {
              $("#step_details").find("#errorMsg").empty(); 
              $("#errorMsg").append("Please fill all the field.") ;
              $("#step_details").find("#name").focus();
            return false;
        }else if($("#step_details").find("#height").val() === "") {
            $("#step_details").find("#errorMsg").empty(); 
              $("#errorMsg").append("Please fill all the field.") ;
              $("#step_details").find("#height").focus();
            return false;
        }else if($("#step_details").find("#weight").val() === "") {
            $("#step_details").find("#errorMsg").empty(); 
              $("#errorMsg").append("Please fill all the field.") ;
              $("#step_details").find("#weight").focus();
            return false;
        }else if($("#step_details").find("#email").val() === "" || !emailfilter.test($("#step_details").find("#email").val())) {
              $("#step_details").find("#errorMsg").empty(); 
              $("#errorMsg").append("Please fill all the field.") ;
               $("#step_details").find("#email").focus();
            return false;
        }
        else if($("#step_details").find("#contact").val() == ""|| !contactnofilter.test($("#step_details").find("#contact").val())) {
                $("#step_details").find("#errorMsg").empty(); 
                $("#errorMsg").append("Please fill all the field.") ;
                $("#step_details").find("#contact").focus();
            return false;
        }
        else if ($("#step_details").find("#location").val() == "") {
                $("#step_details").find("#errorMsg").empty(); 
                $("#errorMsg").append("Please fill all the field.") ;
                $("#step_details").find("#location").focus();
            return false;
        } else {    
            plan_for_yo["name"]=$("#step_details").find("#name").val();
            plan_for_yo["height"]=$("#step_details").find("#height").val();
            plan_for_yo["weight"]=$("#step_details").find("#weight").val();
            plan_for_yo["email"]=$("#step_details").find("#email").val();
            plan_for_yo["contact"]=$("#step_details").find("#contact").val();
            plan_for_yo["location"]=$("#step_details").find("#location").val();
            plan_for_yo["dob"]=$("#step_details").find("#dob").val();
            $('#letsgo').attr("disabled", true); 
            $('#letsgo').html($("<img>").attr({'src':'images/fb_ty.gif'}))
            setTimeout(function (){
                $('#step_details').find('#rbutton').attr("disabled", false); 
                $('#letsgo').attr("disabled", false); 
                $('#letsgo').html("Lets Go");
                $('#letsgo').hide();
                $("#htab1").find("div:first-child").html('<div class="col-md-12 col-xs-12"> <center><p style="font-family:century gothic;text-align:center;margin:18px;font-weight:bold;">Thank you for your time! This is your first step to a transformed you, we will get in touch with you shortly.</p></center> </div>');
            },500);
            $.ajax({
                type:"post",
                url:"server/controller/InsStepDetails.php",
                data:plan_for_yo,
                success: function(data){
                    if(data.trim()==="Success"){
                        $('#success_msg_demo').modal('hide');
                        window.location="index.php";
                    }
                }
            });
        }
    });
});

