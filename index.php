<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
	<!--<![endif]-->
<head>
		<meta charset="utf-8">
		<title>B-Nutrified</title>
		<meta name="description" content="">
		<meta name="author" content="">
                <link rel="shortcut icon" href="images/fevicon.png">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                 <?php include './includes/cssfiles.php';?> 
                <style>
                    .btn-default-transparent:active,.btn-default-transparent:focus{
                        background:none;
                            color: #777;
                    }
                    .tips {
                    display: inline-block;
                    position: relative;
                    width: auto;
                    min-width: 180px;
                    height: auto;
                    vertical-align: top;
                    margin-left: 35px;
                    margin-right: 35px;
                    margin-bottom: 50px;
                    }
                    .innerTip {
                    display: inline-block;
                    vertical-align: middle;
                    width: 100%;
                    margin-left:auto;
                    margin-right:auto;
                    text-align:center;
                    }
                    .innerTipTitle {
                    font-family:century gothic;
                    font-weight:bold;
                    
                    color: #007833;
                    font-size: 16px;
                    line-height: 20px;
                    text-align: center;
                    text-decoration: none;
                    vertical-align: top;
                    padding-top: 10px;
                    }
                    #steps li{
                        font-family:century gothic;
                        font-size:24px;
                        font-weight:bold;
                        
                    }
                    #steps li a{
                        font-family:century gothic;
                        font-size:16px;
                        font-weight:bold;
                        background:transparent;
                       
                       
                    }
                    #steps li a:active{
                        background:orangered;
                        color:white;
                         background:transparent;
                    }
                    #stepContent{
                        position:relative;
                        top:30px;
                        background:#e9ebee;
                    }
                    #stepImg{
                        cursor:pointer;
                    }
                    #stepImg_2{
                        cursor:pointer;
                        height: 200px;
                    }
                    #stepContentInner{
                        background:#e9ebee;
                    }
                                    #footer-top {
                                    background: #0d5995;
                                    min-height: 40px;
                                    border-bottom: 1px solid white;
                                    height: auto;
                                    color: white;
                                    margin-top: 1px;
                                    padding-top: 15px;
                                    box-shadow: inset 0 4px 10px rgba(0,0,0, 0.50);
                                    border-bottom: 1px solid white;
                                    }
                                    #menuBar{
                                        position:relative;
                                        float:right;
                                        top:3px;
                                        font-size:16px;
                                        color:black;
                                        cursor:pointer;
                                        
                                    }
                                    #menuBar:hover{
                                        color:#0d5995;
                                        
                                    }
                                    #request{
                                        position:relative;
                                        margin-right:10px;
                                        margin-left:10px;
                                        font-weight:bold;
                                    }
                                    .scrollToDown{
                                        text-align: center;
                                        color: #fff;
                                        font-size:21px;
                                        font-weight:bold;
                                        position: fixed;
                                        padding-top:10px;
                                        bottom:60px;
                                        right:5px;
                                        width: 50px;
                                        height: 50px;
                                        cursor: pointer;
                                        background-color: rgba(0,0,0,.4);
                                        z-index: 1005;
                                        -webkit-backface-visibility: hidden;
                                        -webkit-transform: translateZ(0);
                                        -webkit-transition: all .2s ease-in-out;
                                        -o-transition: all .2s ease-in-out;
                                        transition: all .2s ease-in-out;
                                    }


                                
                </style>
	</head>

	
	<body class="no-trans  ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
                   
			<!-- Offcanvas side start -->
                       
			<!-- offcanvas side end -->
		
			<!-- banner start -->
			<!-- ================ -->
			<div class="banner clearfix">
                            <?php include 'includes/header.php';?>
				<!-- slideshow start -->
				<!-- ================ -->
                                <div class="slideshow" >
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
         <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from banners";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    $i=0;
                                                                    while($row = $course_batch_details->fetch()){
                                                                                $class="";
                                                                                if($i==0){
                                                                                    $class=$class."active";
                                                                                }
                                                                                
                                                                        ?>
          <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php echo $class; ?>"></li>
                                                                <?php    $i++;}
                                                                ?>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        
        
                                                                <?php
                                                                    include_once 'server/dbhelper/DatabaseHelper.php';
                                                                    $dbh = new DatabaseHelper();
                                                                    $sql = "select * from banners";
                                                                    $stmt = $dbh->createConnection()->prepare($sql);            
                                                                    $stmt->execute();
                                                                    $dbh->closeConnection();
                                                                    $course_batch_details = $stmt;
                                                                    $i=0;
                                                                    while($row = $course_batch_details->fetch()){
                                                                                $class="item";
                                                                                if($i==0){
                                                                                    $class=$class." active";
                                                                                }
                                                                                
                                                                        ?>
        <div class="<?php echo $class; ?>" >
          <img src="server/controller/<?php echo $row['path'];?>" style="width:100%;">
      </div>
                                                                <?php    $i++;}
                                                                ?>
        
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
				
				<!-- slideshow end -->
                       </div>    
			</div>
			<!-- banner end -->
			
			<div id="page-start"></div>

			<!-- section start -->
			<!-- ================ -->
                        
			<section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container">
					<div class="row">
                                                <div class="col-md-3">
                                                    <center>
                                                   <p>&nbsp;</p>
                                                   <p>&nbsp;</p>
                                                   <div class="overlay-container overlay-visible" data-toggle="modal" data-target="#videoModal" style="background:url('images/video-bg.jpg') no-repeat;background-size:cover;height:110px;width:196px;cursor:pointer;box-shadow:0 6px 10px rgba(0,0,0, 0.25);"> </div>
                                                   <p>&nbsp;</p>
                                                   </center>
                                               </div>
						<div class="col-md-6">
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;line-height:36px;">
                                                                <strong>How It Works</strong>                                                        
                                                    
                                                    </h1>
                                                    <div class="separator"></div>
                                                    <p id="cpara">  <p>Ever wondered why you have not been able to transform your body ??</p>

<p>It&#39;s not the methods; it&#39;s the difficulty in finding the one that works for you!</p>

<p>Avoid going through the pain of researching and finding. Let the experts at bnutrified use the formula of the right mix to customize a plan for you and get the body you have been dreaming of!</p>

<p>Achieve more with less effort!</p></p>
                                                        
                                                         <p>&nbsp;</p>
                                               </div>
                                            <div class="col-md-3" >
                                                <center>
                                            <div  style="height:300px;background:url('images/calorie_tracker_elements_right.png') no-repeat;background-size:cover;background-position:center bottom;">
                                                <p>&nbsp;</p>  
					   </div>
                                                    </center>
					   </div>
						
						
					</div>
                                    <p>&nbsp;</p>
                                    
                                    <div class="row">
                                        <div class="img-responsive col-md-3"><center><img src="images/hwt_1.png"/></center></div>
                                             <div class="img-responsive col-md-3"><center><img src="images/hwt_2.png"/></center></div>
                                             <div class="img-responsive col-md-3"><center><img src="images/hwt_3.png"/></center></div>
                                             <div class="img-responsive col-md-3"><center><img src="images/hwt_4.png"/></center></div>
                                    </div>
                
                                   
                                 </div>
                        </section>
                        
			<div class="section pv-40 parallax dark-translucent-bg background-img-3" style="box-shadow:inset 0 5px 10px rgba(0,0,0, 0.50);background-position: 50% -22px;">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
                                                    <div class="separator"></div>
                                                    <p style="text-align:center;font-family:century gothic;line-height:36px;font-size:32px;">Get a quote for your own customized plan.</p>
						 <div class="separator"></div>
							
						</div>
					</div>
				</div>
			</div>
                      <section class="section pv-40 parallax dark-translucent-bg background-img-6" style="box-shadow:inset 0 5px 10px rgba(0,0,0, 0.50);background-position: 50% -22px;">
                            <div class="container">
					<div class="row">
                                            <div class="col-md-3">
                                                <p>&nbsp;</p>  
					   </div>
						<div class="col-md-7">
                                                    
                                                    <h1 class="text-center " id="heading-font" style="text-transform:none;line-height:36px;">
                                                               
                                                        <strong>Let’s take that first step!</strong>                                                        
                                                    </h1>
                                                    <div class="separator"></div>
                                                    <p id="cpara" style="font-size:18px;font-weight:bold;">You are unique, you need unique! Fill the form to access a healthy you:</p><p>&nbsp;</p>
                                                      
                                               </div>
                                                <div class="col-md-2">
                                                    <p>&nbsp;</p>  
                                                  
                                                </div>
						
					</div>
                                    
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-6" >
                                             <div class="col-md-1"></div>
                                            <div class="main col-md-11 col-xs-12" style="background:rgba(0,0,0,0.3);box-shadow: 0 4px 10px rgba(0,0,0, 0.50);padding:0px;" >
                                                      <ul class="nav nav-tabs style-1" role="tablist" id ="steps">
                                                          <li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-user pr-10" ></i><b id="title_list_tab">Select Your Gender</b></a></li>
<!--								<li class=""><a href="#htab2" role="tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-line-chart pr-10"></i>Select Your Goal</a></li>
								<li class=""><a href="#htab3" role="tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-asterisk pr-10"></i>Select Your Activity Level</a></li>
								<li class=""><a href="#htab3" role="tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-calendar pr-10"></i>Select Your Schedule</a></li>-->
							</ul>
							<!-- Tab panes -->
							<div class="tab-content">
								<div class="tab-pane fade active in" id="htab1">
                                                                        
									<div class="row">
                                                                
									</div>
                                                                    <center><button href="#" class="btn btn-lg btn-gray-transparent " style=""onclick="setReset()">Reset</button>
                                                                        <button style="margin-left: 15px;display: none;" class="btn btn-lg btn-gray-transparent" id="letsgo"  >Lets Go</button></center>
								</div>
							</div>
							
						
						</div>
                                              
                                        </div>
                                         <div class="col-md-3"></div>  
                                    </div>
                                 </div>
                        </section>
                        <div class="section pv-40 parallax dark-translucent-bg background-img-3" style="box-shadow:inset 0 5px 10px rgba(0,0,0, 0.50);background-position: 50% -22px;">
                            <div class="space-bottom">
                                <div class="owl-carousel content-slider" id="SelTestimonials">
                                   
                            <?php
    include_once 'server/dbhelper/DatabaseHelper.php';
    $dbh = new DatabaseHelper();
    $sql = "select * from feedback";
    $stmt = $dbh->createConnection()->prepare($sql);            
    $stmt->execute();
    $dbh->closeConnection();
    $feedback = $stmt;
    $str = array();    
    while($row = $feedback->fetch()){ ?>
        <div class="container">
                                        <div class="row">
                                            <div class="col-md-8 col-md-offset-2">
                                                <div class="testimonial text-center">
                                                    <div class="testimonial-image">
                                                        <img src="server/controller/<?php echo $row['image']; ?>" style="height:70px;width:70px;" alt="" title="" class="img-circle">
                                                    </div>
                                                    <div class="testimonial-body">
                                                        <p id="cpara"><?php echo $row['message'];?></p>
                                                        <div class="cpara3"><?php echo $row['name'];?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
    <?php }
    ?>
                                </div>
                                    
                            </div>
                        </div>
                        <section class="light-gray-bg pv-30 clearfix" id="homeRow1">
                            <div class="container">
					<div class="row">
                                           <h1 class="text-center " id="heading-font" style="text-transform:none;line-height:36px;">
                                                                <strong>For more info, you may also call us on</strong>
                                                                <br/>
                                                                 <br/>
                                                    
                                                    </h1>
                                            <p id="phone_no" style="font-size:56px;text-align:center;font-family:century gothic;color:#333030;line-height:56px;">09008688882</p>
					</div>
                               
                                    <p>&nbsp;</p>
                                 
                                 </div>
                        </section>
                      <?php include 'includes/footer.php'; ?>
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<?php include 'includes/jsfiles.php'; ?>
                <script type="text/javascript" src="ajax/QuickQuery.js"></script>
<?php include 'includes/support.php'; ?>
 
        
        
        <script>
            var check = true;
            $("body").click(function(){
                  if(!check){
                    closeNav(); 
                }
                   
                  
               });
            var plan_for_yo={};
            $(document).ready(function() {
               
               setReset();
            });
            function setReset(){
                plan_for_yo={};
                $("#steps").html('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-user pr-10" ></i><b id="title_list_tab">Select Your Gender</b></a></li>');
                $("#htab1").find("div:first-child").html('<div class="col-md-6 col-xs-6"><center><img onclick="setMale1();" src="images/icon/b-nutirifed-1.png" id="stepImg"/></center><p style="font-family:century gothic;text-align:center;margin-top:10px;font-weight:bold;" >Male</p></div><div class="col-md-6 col-xs-6"><center><img onclick="setFeMale1();" src="images/icon/b-nutirifed-2.png" id="stepImg"/></center><p style="font-family:century gothic;text-align:center;margin-top:10px;font-weight:bold;">Female</p> </div>');
                $("#letsgo").hide();    
            }
            function setMale1(){
                plan_for_yo['gender']="male";
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-male pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Select Your Goal");
                $("#htab1").find("div:first-child").html('<div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_loss();" src="images/icon/b-nutirifed-5.png" id="stepImg_2"/></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_mgmt();" src="images/icon/b-nutirifed-7.png" id="stepImg_2"/></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_gain();" src="images/icon/b-nutirifed-9.png" id="stepImg_2"/></center></div>');
            }
             function setFeMale1(){
                plan_for_yo['gender']="Female";
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-female pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Select Your Goal");
                $("#htab1").find("div:first-child").html('<div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_loss();" src="images/icon/b-nutirifed-3.png" id="stepImg_2" /></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_mgmt();" src="images/icon/b-nutirifed-6.png" id="stepImg_2" /></center></div><div class="col-md-4 col-xs-4"><center><img onclick="setMale1_weight_gain();" src="images/icon/b-nutirifed-8.png" id="stepImg_2" /></center></div>');
            }
            function setMale1_weight_loss(){
                plan_for_yo['goal']="weight_loss";
                setMale_activity();
            }
            function setMale1_weight_mgmt(){
                plan_for_yo['goal']="weight_mgmt";
                setMale_activity();
            }
            function setMale1_weight_gain(){
                plan_for_yo['goal']="weight_gain";
                setMale_activity();
            }
            function setMale_activity(){
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-bullseye pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Enrolled for any fitness activity ");
                $("#htab1").find("div:first-child").html('<div class="col-md-12 col-xs-8"> <center> <div class="form-group"> <label for="inputEmail3" class="control-label"></label> <br><div class="btn-group btn-toggle"> <button class="btn btn-xs btn-default-transparent" style="color:white" onclick="setOn()">Yes</button> <button class="btn btn-xs btn-default-transparent"style="color:white" onclick="setOff()">No</button> </div></div><div class="form-group" style="display:none"> <label for="inputEmail3" class="control-label">Please mention the Type of fitness in the below field</label> <input type="text" class="form-control" style="width:90%" id="id_on_off" placeholder="Gym, yoga”" disabled="true"> </div><div class="form-group"> <button href="#" class=" btn btn-lg btn-gray-transparent " id="id_next" onclick="setContactForm()" style="display:none;">Next</button> </div><label class="control-label" id="error_msg_id"></label> </center></div>');
            }
            var toogle=false;
            function setOn(){
                toogle=true;
                $("#id_on_off").parent().css({'display':'block'})
                $("#id_next").css({'display':'block'})
                $("#id_on_off").prop("disabled",false);
            }
            function setOff(){
                toogle=false;
                $("#id_on_off").parent().css({'display':'none'})
                $("#id_on_off").prop("disabled",true);
                plan_for_yo['fitness_activity']="No";
                setContactForm();
            }
            plan_for_yo['fitness_activity']="No";
            function setContactForm(){
                if(toogle){
                   if($("#id_on_off").val()!="No"){
                        plan_for_yo['fitness_activity']=$("#id_on_off").val();
                   }else{
                       $("#error_msg_id").html("Please mention the Type of fitness .").css({'color':'red'});
                       return false;
                   } 
                }
//                alert(JSON.stringify(plan_for_yo))
                $('<li class="active"><a href="#htab1" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa  fa-check pr-10"></i></li>').insertBefore($("#title_list_tab").parent().parent());
                $("#title_list_tab").html("Contact ");
                $("#htab1").find("div:first-child").html($('<div class="col-md-12"> <form style="margin-right: 19px;" class="form-horizontal" role="form" id="step_details"> <center> <h3>About You</h3></center> <div class="form-group"></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Name</label> <div class="col-sm-9"> <input type="text" class="form-control" id="name" name="name" placeholder="Your Name *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Heigh (Ft.)</label> <div class="col-sm-9"> <input type="number" class="form-control" id="height" name="height" placeholder="Your height *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Weight (Kg)</label> <div class="col-sm-9"> <input type="number" class="form-control" id="weight" name="weight" placeholder="Your weight *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Email</label> <div class="col-sm-9"> <input type="email" id="email" name="email" class="form-control" placeholder="Email *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Contact No</label> <div class="col-sm-9"> <input type="text" id="contact" name="contact" class="form-control" placeholder="Contact *"> </div></div><div class="form-group"> <label for="inputPassword3" class="col-sm-3 control-label">Location</label> <div class="col-sm-9"> <input type="text" class="form-control" id="location" name="location" placeholder="Location eg. Koramangala *"> </div></div><div class="form-group"> <label for="inputEmail3" class="col-sm-3 control-label">Date of Birth</label> <div class="col-sm-9"> <input type="date" class="form-control" id="dob" name="dob"> </div></div><div class="form-group"> <center> <p id="errorMsg" style="color:red;font-family:verdana;font-weight:bold"></p></center> </div></form></div>'));
                $("#letsgo").show();
            }
        </script>
        <script type="text/javascript" src="ajax/Step.js"></script>
        
        
</html>
