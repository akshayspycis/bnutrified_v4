-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2017 at 09:32 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bnutrified`
--

-- --------------------------------------------------------

--
-- Table structure for table `quick_query`
--

CREATE TABLE IF NOT EXISTS `quick_query` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `contact` varchar(200) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `quick_query`
--

INSERT INTO `quick_query` (`id`, `name`, `email`, `contact`, `location`, `date`) VALUES
(8, NULL, NULL, NULL, 'SD', NULL),
(9, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(10, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(11, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(12, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(13, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(14, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(15, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(16, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(17, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(18, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(19, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(20, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(21, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(22, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(23, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(24, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(25, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(26, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(27, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(28, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(29, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(30, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(31, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(32, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(33, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(34, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(35, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(36, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Sat 13 May 2017'),
(37, 'kjj', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(38, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(39, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(40, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(41, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(42, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(43, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(44, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(45, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(46, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(47, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017'),
(48, 'klk', 'klkk@sd.com', '8817919016', 'SD', 'Fri 12 May 2017');

-- --------------------------------------------------------

--
-- Table structure for table `step_details`
--

CREATE TABLE IF NOT EXISTS `step_details` (
  `step_details_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gender` varchar(200) DEFAULT NULL,
  `goal` varchar(200) DEFAULT NULL,
  `fitness_activity` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `contact` varchar(200) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `dob` varchar(200) DEFAULT NULL,
  `date` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`step_details_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `step_details`
--

INSERT INTO `step_details` (`step_details_id`, `gender`, `goal`, `fitness_activity`, `name`, `email`, `contact`, `location`, `dob`, `date`) VALUES
(1, 'male', 'weight_mgmt', '', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-11-30', 'Fri 12 May 2017'),
(2, 'male', 'weight_mgmt', 'OK', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2017-05-11', 'Fri 12 May 2017'),
(3, 'Female', 'weight_gain', 'OLO', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2017-05-10', 'Fri 12 May 2017'),
(4, 'male', 'weight_gain', 'OAO', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-11-30', 'Fri 12 May 2017'),
(5, 'male', 'weight_mgmt', '', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-09-30', 'Fri 12 May 2017'),
(6, 'Female', 'weight_mgmt', 'No', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-11-29', 'Fri 12 May 2017'),
(7, 'male', 'weight_mgmt', '', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2015-11-29', 'Fri 12 May 2017'),
(8, 'Female', 'weight_mgmt', 'No', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-11-30', 'Fri 12 May 2017'),
(9, 'Female', 'weight_gain', '', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-11-29', 'Fri 12 May 2017'),
(10, 'Female', 'weight_mgmt', '', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-11-30', 'Fri 12 May 2017'),
(11, 'male', 'weight_mgmt', '', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-11-29', 'Fri 12 May 2017'),
(12, 'male', 'weight_mgmt', 'No', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-11-29', 'Fri 12 May 2017'),
(13, 'male', 'weight_mgmt', 'No', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2016-10-28', 'Fri 12 May 2017'),
(14, 'male', 'weight_mgmt', 'NI hai', 'klk', 'klkk@sd.com', '8817919016', 'SD', '2015-11-28', 'Fri 12 May 2017');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
